<?php
declare(strict_types=1);

namespace App\Controllers;

use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class Home extends BaseController
{
    /**
     * @return ResponseInterface
     * @throws ServerInternalError
     */
    public function index(): ResponseInterface
    {
        return $this->sendTwig('home.php');
    }
}

