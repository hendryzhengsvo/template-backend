<?php

namespace App\Controllers\Admin;

use App\Modules\Storage;
use App\Controllers\BaseController;

/**
 * Class Initial
 */
class Initial extends BaseController
{
    public function index()
    {
        return $this->sendDone([
            'initial'  => TRUE,
            'admin'    => Storage::$admin ? Storage::$admin->toFrontend() : FALSE,
            'config'   => $this->modules->config->frontendConfig->all(Storage::$platform),
            'leftMenu' => $this->modules->admin->role->getMenu(Storage::$admin->roleId ?? 0),
            'topMenu'  => $this->getTopMenu(),
        ]);
    }

    private function getTopMenu()
    {
        return $this->modules->config->frontendConfig->get('admin_top_menu') ?? [
            [
                "title"     => "Home",
                "name"      => "dashboard",
                "component" => "dashboardIcon",
                "to"        => $this->modules->config->frontendConfig->get('admin_home_route')['name'] ?? 'Admin/Home',
            ],
        ];
    }
}