<?php
declare(strict_types=1);

namespace App\Controllers\Admin\Auth;

use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class Facebook extends BaseAuth
{
    protected string $platform = 'facebook';

    /**
     * @return ResponseInterface
     */
    public function login(): ResponseInterface
    {
        return $this->sendDone(['url' => $this->genSocialLoginUrl()]);
    }

    /**
     * @return ResponseInterface
     * @throws ServerInternalError
     */
    public function confirm(): ResponseInterface
    {
        return $this->joinAdmin();
    }

    /**
     * @return RedirectResponse
     */
    public function redirect(): RedirectResponse
    {
        return redirect()->to($this->connectAdmin());
    }

}