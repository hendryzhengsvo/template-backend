<?php
declare(strict_types=1);

namespace App\Controllers\Admin\Auth;

use Config\Services;
use ReflectionException;
use App\Libraries\Provider;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\HTTP\ResponseInterface;
use RobThree\Auth\TwoFactorAuthException;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ClientBadRequest;

/**
 * Class Reset
 */
class Reset extends BaseController
{
    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     */
    public function requestReset(): ResponseInterface
    {
        $lang = Services::language()->getLocale() ?? 'en';

        if(!$this->validate(['email' => 'valid_email']))
            throw new ClientBadRequest('', 'Exceptions.Please provide a valid email address.');

        $email = $this->request->getVar('email');

        //check if multiple submit
        $redisKey   = sha1('adminResetPassword:' . $email);
        $redis      = Services::redis();
        $retryCount = (int)$redis->getUnserialize($redisKey);
        //update sent
        $redis->setSerialize($redisKey, 1, 30);
        if($retryCount > 0)
            return $this->sendDone('ok');

        if(!$admin = $this->modules->admin->getByEmail($email)) {
            $redis->del($redisKey);
            throw new ClientBadRequest('', 'Exceptions.Admin not found.');
        }

        $code     = $this->generateKey($email);
        $frontend = $this->request->getVar('frontend');
        $url      = "$frontend#" . http_build_query(["code" => $code, "email" => $admin->email]);
        $subject  = lang("General.Admin Panel Account Password Reset Request Confirmation - Action Required");
        $result   = $this->modules->admin->sendAdminEmail('reset', ['link' => $url], $subject, $admin->email, $lang);
        $return   = !isProduction() ? ['url' => $url, 'result' => $result] : NULL;
        return $this->sendDone($return);
    }

    /**
     * @param string $email
     * @return string
     */
    private function generateKey(string $email): string
    {
        $action = "password-reset:$email";
        return substr(sha1($email), 0, -6) . Services::twoFactor()::getCode($action);
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     */
    public function requestOtp(): ResponseInterface
    {
        if(!$this->validate(['email' => 'valid_email']))
            throw new ClientBadRequest('', 'Exceptions.Please provide a valid email address.');
        elseif(Services::redis()->get('resetTimeout') ?? 0)
            return $this->sendDone('ok');

        $email = $this->request->getVar('email');
        if(!$admin = $this->modules->admin->getByEmail($email))
            throw new ClientBadRequest('', 'Exceptions.Admin not found.');

        $code    = $this->generateKey($email);
        $subject = lang("Info.Admin Panel reset password");
        $content = lang("Info.Your password reset code is:{code}", ['code' => $code]);
        $from    = [
            'Name'  => config('Email')->{'fromName'},
            'Email' => config('Email')->{'fromEmail'},
        ];
        $to      = [
            [
                'Name'  => $admin->name,
                'Email' => $admin->email,
            ]];
        $result  = Provider::sendEmail($subject, $content, $from, $to);
        if($result) Services::redis()->setex('resetTimeout', 1, 60);
        $return = !isProduction() ? ['code' => $code] : NULL;
        return $this->sendDone($return);
    }

    /**
     * @return RedirectResponse
     * @throws ClientBadRequest
     * @throws TwoFactorAuthException
     */
    public function redirect(): RedirectResponse
    {
        $id       = (int)$this->request->getVar('id');
        $code     = (string)$this->request->getVar('code');
        $frontend = (string)$this->request->getVar('frontend');
        if(!$this->validate(['code' => 'exact_length[40]', 'id' => 'integer', 'frontend' => 'valid_url']))
            throw new ClientBadRequest('', 'Exceptions.Forbidden denied');

        if(!$admin = $this->modules->admin->get($id))
            throw new ClientBadRequest('', 'Exceptions.Admin not found.');
        elseif(!$this->verifyKey($admin->email, $this->request->getVar('code')))
            throw new ClientBadRequest('', 'Exceptions.Admin not found.');
        return redirect()->to("$frontend#" . http_build_query(["code" => $code, "email" => $admin->email]));
    }

    /**
     * @param string $email
     * @param string $key
     * @return bool
     * @throws TwoFactorAuthException
     */
    private function verifyKey(string $email, string $key): bool
    {
        [$key, $code] = str_split($key, 34);
        $action = "password-reset:$email";
        return substr(sha1($email), 0, -6) === $key
            && Services::twoFactor()::verify($action, $code, 144);
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws TwoFactorAuthException
     */
    public function resetPassword(): ResponseInterface
    {
        if(!$this->validate([
            'code'     => 'exact_length[40]',
            'email'    => 'valid_email',
            'password' => 'required',//"regex_match[/$pattern/]"
        ]))
            throw new ClientBadRequest('', 'Exceptions.Please provide a valid password.');
        elseif(!$admin = $this->modules->admin->getByEmail($this->request->getVar('email')))
            throw new ClientBadRequest('', 'Exceptions.Admin not found.');

        $code = $this->request->getVar('code');
        if(!$this->verifyKey($admin->email, $code))
            throw new ClientBadRequest('', 'Exceptions.Admin not found.');
        $this->modules->admin->setPassword($admin, $this->request->getVar('password'));
        Services::redis()->del('resetTimeout');
        return $this->sendDone('ok');
    }

}