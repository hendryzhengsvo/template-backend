<?php
declare(strict_types=1);

namespace App\Controllers\Admin\Auth;

use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class Google extends BaseAuth
{
    protected string $platform = 'google';

    /**
     * @return ResponseInterface
     */
    public function login(): ResponseInterface
    {
        return $this->sendDone(['url' => $this->genSocialLoginUrl('none')]);
    }

    /**
     * @return ResponseInterface
     * @throws ServerInternalError
     */
    public function confirm(): ResponseInterface
    {
        return $this->joinAdmin();
    }

    /**
     * @return RedirectResponse
     */
    /**
     * @return RedirectResponse
     */
    public function redirect(): RedirectResponse
    {
        $state = (string)$this->request->getVar('state');
        if(!empty($state)) {
            $queryData = [];
            parse_str($state, $queryData);
            $this->request->setGlobal('request', array_merge($this->request->fetchGlobal('request'), $queryData));
        }
        $error = (string)$this->request->getVar('error');
        if($error === 'immediate_failed')
            return redirect()->to($this->genSocialLoginUrl('consent'));
        return redirect()->to($this->connectAdmin());
    }
}