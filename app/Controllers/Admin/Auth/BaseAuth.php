<?php

namespace App\Controllers\Admin\Auth;

use Throwable;
use Config\Services;
use App\Libraries\Profiler;
use App\Libraries\Utilities;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class BaseAuth extends BaseController
{
    protected string $panel    = 'admin';
    protected string $platform = '';

    /**
     * @param string $prompt
     * @return string
     */
    protected function genSocialLoginUrl(string $prompt = ''): string
    {
        $callbackUrl  = Utilities::getFullUrl();
        $callbackUrl  = preg_replace('#/login$#', '/redirect', $callbackUrl);
        $action       = (string)$this->request->getVar('action');
        $frontend     = (string)$this->request->getVar('frontend');
        $type         = (string)$this->request->getVar('type');
        $referralCode = (string)$this->request->getVar('referral_code');
        return $this->modules->auth->oAuth->{$this->panel . ucfirst($this->platform)}->getOAuthUrl($callbackUrl, http_build_query(['action' => $action, 'type' => $type, 'referral_code' => $referralCode, 'frontend' => $frontend, 'lang' => Services::language()
            ->getLocale()]), $prompt);
    }

    /**
     * @return ResponseInterface
     * @throws ServerInternalError
     */
    protected function joinAdmin(): ResponseInterface
    {
        $key       = (string)$this->request->getVar('key');
        $oauthUser = $key ? Services::redis()->getUnserialize($key) : NULL;
        if($oauthUser && ($admin = $this->modules->admin->getByEmail($oauthUser['email']))) {
            $this->modules->auth->oAuth->{$this->panel . ucfirst($this->platform)}->addConnect($admin->id, $oauthUser['id']);
            $token = $this->modules->auth->admin->generateToken($admin);
            Services::redis()->expire($key, 1);
            return $this->sendDone(['token' => $token]);
        } else
            return $this->sendError('General.Register Failed', '', NULL,
                ['name' => 'Auth/Home', 'delay' => 3000]);
    }

    /**
     * @return string
     */
    protected function connectAdmin(): string
    {
        $type    = 'login';
        $fullUrl = '';
        try {
            $state = (string)$this->request->getVar('state');
            if(!empty($state)) {
                $queryData = [];
                parse_str($state, $queryData);
                $this->request->setGlobal('request', array_merge($this->request->fetchGlobal('request'), $queryData));
            }
            $code  = (string)$this->request->getVar('code');
            $error = (string)$this->request->getVar('error');
            $url   = (string)$this->request->getVar('frontend');
            $lang  = (string)$this->request->getVar('lang') ?? 'en';
            Services::language()->setLocale($lang);
            $fullUrl = "$url#";

            //Check error
            if(!empty($error) && empty($code))
                return $fullUrl . http_build_query(['action' => 'error', 'message' => $error]);

            //Parse code
            if(!($oauthUser = $this->modules->auth->oAuth->{$this->panel . ucfirst($this->platform)}->parseCode($code)))
                return $fullUrl . http_build_query(['action' => 'error', 'message' => lang('Exception.Service Unavailable')]);

            //Check connect
            if(!($connect = $this->models->oauthConnect->getByParty($this->panel, $this->platform))) {
                if(!empty($oauthUser['email'])) {
                    if(($admin = $this->modules->admin->getByEmail($oauthUser['email']))) {
                        if($admin->status === 1) {
                            $key = sha1(Utilities::genUuidV4());
                            Services::redis()->setSerialize($key, $oauthUser, 600);
                            return $fullUrl . http_build_query(['action' => 'confirm', 'panel' => $this->panel, 'platform' => $this->platform, 'key' => $key]);
                        } else
                            return $fullUrl . http_build_query(['action' => 'error', 'message' => lang('Exception.Status inactive')]);
                    } elseif($type !== 'login')
                        $admin = $this->modules->auth->oAuth->{$this->panel . ucfirst($this->platform)}->connect($oauthUser);
                    else
                        return $fullUrl . http_build_query(['action' => 'error', 'message' => lang('Exception.Service Unavailable')]);
                } else
                    return $fullUrl . http_build_query(['action' => 'register', 'platform' => 'facebook', 'open_id' => $oauthUser['id'], 'name' => $oauthUser['name']]);

                //Check admin by connect, register new admin if not exist
            } elseif(!($admin = $this->modules->admin->get($connect->roleid))) {
                if($type === 'login')
                    return $fullUrl . http_build_query(['action' => 'error', 'message' => lang('Exception.Service Unavailable')]);
                elseif(empty($oauthUser['email']))
                    return $fullUrl . http_build_query(['action' => 'register', 'panel' => $this->panel, 'platform' => $this->platform, 'open_id' => $oauthUser['id'], 'name' => $oauthUser['name']]);
            }

            if($admin && $admin->status !== 1)
                return $fullUrl . http_build_query(['action' => 'error', 'message' => lang('Exception.Status inactive')]);

            //Generate token, login admin
            $this->modules->auth->admin->logLogin($admin->id);
            $token = $this->modules->auth->admin->generateToken($admin);
            return $fullUrl . http_build_query(['action' => 'login', 'token' => $token]);

        } catch(Throwable $ex) {
            Services::reporter()->debug("social_connect:$this->panel:$this->platform", [
                'status'    => 'exception',
                'app'       => APP_NAME ?? '',
                'domain'    => $_SERVER['HTTP_HOST'] ?? '',
                'type'      => __CLASS__,
                'exception' => [
                    'message' => $ex->getMessage(),
                    'trace'   => $ex->getTraceAsString(),
                ],
                'profiler'  => Profiler::fetch(),
            ]);
            syslog(LOG_ERR, json_encode([
                'status'    => 'exception',
                'app'       => APP_NAME ?? '',
                'domain'    => $_SERVER['HTTP_HOST'] ?? '',
                'type'      => __CLASS__,
                'exception' => [
                    'message' => $ex->getMessage(),
                    'trace'   => $ex->getTraceAsString(),
                ],
                'profiler'  => Profiler::fetch(),
            ], JSON_UNESCAPED_UNICODE));
            return $fullUrl . http_build_query(['action' => 'error', 'message' => $ex->getMessage()]);
        }
    }
}