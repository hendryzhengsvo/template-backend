<?php
declare(strict_types=1);

namespace App\Controllers\Admin\Auth;

use Config\Services;
use App\Modules\Storage;
use ReflectionException;
use App\Controllers\BaseController;
use lbuchs\WebAuthn\WebAuthnException;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ClientBadRequest;
use Wellous\Ci4Component\Exceptions\ClientNotAcceptable;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class Login extends BaseController
{
    /**
     * @return ResponseInterface
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws ClientNotAcceptable
     */
    public function doLoginToken(): ResponseInterface
    {
        $token = $this->request->getJsonVar('token');
        $admin = $this->modules->auth->admin->loginByToken($token);
        return $this->sendDone([
            'admin'     => $admin->toFrontend(),
            'homeRoute' => $this->modules->config->frontendConfig->get('admin_home_route', ['name' => 'Admin/Home']),
        ]);

    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws WebAuthnException
     */
    public function doBioMetricLogin(): ResponseInterface
    {
        $credential = $this->request->getJSON(TRUE);
        if($admin = $this->modules->auth->admin->loginByBiometric($credential)) {
            $this->modules->auth->admin->logLogin($admin->id);
            Storage::$admin = $admin;
            return $this->sendDone([
                'admin'     => $admin->toFrontend(),
                'homeRoute' => $this->modules->config->frontendConfig->get('admin_home_route', ['name' => 'Admin/Home']),
            ]);
        }
        throw new ClientBadRequest('', 'Exceptions.Biometric not found');
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function doLogin(): ResponseInterface
    {
        $email    = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        //check if multiple submit
        $redisKey   = 'adminLogin:' . $email;
        $redis      = Services::redis();
        $retryCount = (int)$redis->getUnserialize($redisKey);
        if($retryCount > 5)
            throw new ClientBadRequest('', 'Exceptions.Reach Maximum login failed retry count');
        //update login failed count
        $redis->setSerialize($redisKey, ++$retryCount, 600);
        if($admin = $this->modules->auth->admin->login($email, $password)) {
            $this->modules->auth->admin->logLogin($admin->id);
            $redis->del($redisKey);
            return $this->sendDone([
                'admin'     => $admin->toFrontend(),
                'homeRoute' => $this->modules->config->frontendConfig->get('admin_home_route', ['name' => 'Admin/Home']),
            ]);
        }
        throw new ClientBadRequest('', 'Exceptions.Username/Password is wrong');
    }

    /**
     * @return ResponseInterface
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws ServerInternalError
     */
    public function doLogout(): ResponseInterface
    {
        $this->modules->auth->admin->logout();
        return $this->sendSuccess('General.Logout successfully.', route: '/auth');
    }
}