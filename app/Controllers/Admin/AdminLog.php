<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

/**
 * Class Initial
 */
class AdminLog extends BaseController
{
    /**
     * @return ResponseInterface
     */
    public function listing(): ResponseInterface
    {
        $filter = (array)$this->request->getJsonVar('filter') ?? [];

        return $this->sendDone([
            'data'       => $this->modules->admin->adminLog->listing($filter),
            'pagination' => $this->modules->pagination->data(),
        ]);
    }
}