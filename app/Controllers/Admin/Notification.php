<?php
declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Modules\Storage;
use ReflectionException;
use CodeIgniter\HTTP\ResponseInterface;
use App\Entities\AdminNotificationEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ClientBadRequest;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class Notification extends Base
{
    /**
     * @return ResponseInterface
     */
    public function getUnreadCount(): ResponseInterface
    {
        $categories = new AdminNotificationEntity();
        $categories = $categories->getCategories();
        $result     = array_fill_keys($categories, ['count' => 0, 'content' => '']);
        $total      = 0;
        $count      = $this->models->adminNotification->getUnreadCount(Storage::$admin->id);
        foreach($categories as $cat) {
            $num                     = isset($count[$cat]) ? (int)$count[$cat] : 0;
            $result[$cat]['count']   = $num;
            $total                   += $num;
            $contentInfo             = $this->models->adminNotification->getAdminLastContent(Storage::$admin->id, $cat);
            $result[$cat]['content'] = $contentInfo ? $this->modules->notification->translateParams($contentInfo->content, $contentInfo->params) : '';
        }
        return $this->sendDone([
            'total'         => $total,
            'notifications' => $result,
        ]);
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function getByCategory(): ResponseInterface
    {
        $adminNotificationEntity = new AdminNotificationEntity();
        $categories              = implode(',', $adminNotificationEntity->getCategories());
        if(!$this->validate([
            'category' => "required|in_list[$categories]",
        ])) {
            throw new ClientBadRequest('', 'Exceptions.Bad request');
        }
        $page        = $this->getCurrentPage();
        $limit       = 10;
        $offset      = ($page > 1) ? (($page - 1) * $limit) : 0;
        $category    = $this->request->getVar('category');
        $data        = $this->modules->notification->getCategorizeNotification($category, $limit, $offset);
        $totalRecord = $this->models->adminNotification->getCountByCategory(Storage::$admin->id, $category);
        $this->modules->pagination->init($totalRecord);
        return $this->sendDone([
            'pagination' => $this->modules->pagination->data(),
            'data'       => $data,
        ]);
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function subscribeWebPush(): ResponseInterface
    {
        $subscription = (array)$this->request->getJsonVar('subscription');
        if(empty($subscription))
            throw new ClientBadRequest('', 'Exceptions.Bad request');

        $this->modules->notification->subscribeWebPush(Storage::$admin->id, $subscription);
        return $this->sendDone();
    }

    /**
     * @return ResponseInterface
     * @throws ServerInternalError
     */
    public function unsubscribeWebPush(): ResponseInterface
    {
        $this->modules->notification->unsubscribeWebPush(Storage::$admin->id);
        return $this->sendSuccess(lang('Admin.Notification.unSubscribed'));
    }
}