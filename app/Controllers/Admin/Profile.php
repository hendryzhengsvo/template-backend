<?php
declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Modules\Storage;
use ReflectionException;
use lbuchs\WebAuthn\WebAuthnException;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ClientBadRequest;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class Profile extends Base
{
    /**
     * @return ResponseInterface
     */
    public function details(): ResponseInterface
    {
        $admin = Storage::$admin->toArray();
        unset($admin['password']);
        unset($admin['deletedAt']);
        unset($admin['expired']);
        return $this->sendDone($admin);
    }

    /**
     * @return ResponseInterface
     */
    public function info(): ResponseInterface
    {
        return $this->sendDone(Storage::$admin->toFrontend());
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws ServerInternalError
     */
    public function updateProfile(): ResponseInterface
    {
        $id     = Storage::$admin->id;
        $params = $this->request->getVar();
        $params = is_object($params) ? (array)$params : $params;
        if(!empty($params['ic_no']) && !$this->validate([
                'ic_no' => "is_unique[admin.ic_no,id,$id]",
            ]))
            throw new ClientBadRequest('Invalid_param', 'Exceptions.IC No. already exists.');
        $this->modules->admin->updateAdminProfile($params);
        return $this->sendSuccess("General.Update Successfully.");
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws ServerInternalError
     */
    public function uploadAvatar(): ResponseInterface
    {
        if(!($file = $this->request->getFile('avatar')))
            throw new ClientBadRequest('Invalid_param', 'Exceptions.Invalid file.');

        $url = $this->modules->admin->uploadAvatar($file);
        sleep(5);//in case s3 cache
        return $this->sendSuccess("General.Upload Successfully.", data: ['url' => $url]);
    }

    /**
     * @return ResponseInterface
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws ServerInternalError
     * @throws WebAuthnException
     */
    public function addBioMetric(): ResponseInterface
    {
        $credential = $this->request->getJSON(TRUE);
        if($this->modules->auth->admin->addBiometric(Storage::$admin->id, $credential))
            return $this->sendSuccess("Success add your biometric credential.");
        else
            return $this->sendError("Failed to add your biometric credential.");
    }

    /**
     * @return ResponseInterface
     * @throws ServerInternalError
     */
    public function removeBioMetric(): ResponseInterface
    {
        $credential = $this->request->getJSON(TRUE);
        if($this->modules->auth->admin->removeBioMetric(Storage::$admin->id, $credential))
            return $this->sendSuccess("Success remove your biometric credential.");
        else
            return $this->sendError("Failed to remove your biometric credential.");
    }
}
