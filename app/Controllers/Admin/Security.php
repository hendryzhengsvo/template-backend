<?php
declare(strict_types=1);

namespace App\Controllers\Admin;

use Config\Services;
use App\Modules\Storage;
use ReflectionException;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ClientBadRequest;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class Security extends Base
{
    /**
     * @return Response|ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws ServerInternalError
     */
    public function changePass(): ResponseInterface|Response
    {
        if(!$this->validate([
            'curPass'     => 'required',
            'newPass'     => "required|min_length[6]|max_length[20]",//"regex_match[/$pattern/]"
            'confirmPass' => 'required',
        ])) {
            throw new ClientBadRequest('Invalid params', 'Exceptions.Please check you input.');
        }
        $currentPass = $this->request->getVar('curPass');
        $newPass     = $this->request->getVar('newPass');
        $confirmPass = $this->request->getVar('confirmPass');

        if(Storage::$admin->password !== sha1(Storage::$admin->email . $currentPass))
            throw new ClientBadRequest('Invalid params', 'Exceptions.Invalid Current Password.');
        if($newPass !== $confirmPass)
            throw new ClientBadRequest('Invalid params', 'Exceptions.New Password and Confirm Password are not same.');

        Storage::$admin->password = sha1(Storage::$admin->email . $newPass);
        if(!$this->models->admin->save(Storage::$admin))
            throw new ClientBadRequest('Invalid params', 'Exceptions.Update failed.');
        Services::redis()->del('admin:' . Storage::$admin->id);
        return $this->sendSuccess(lang("General.Update Successfully."));
    }
}