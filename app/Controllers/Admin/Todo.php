<?php
declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Modules\Storage;
use ReflectionException;
use App\Entities\TodosEntity;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ClientBadRequest;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

class Todo extends Base
{
    private string $type = 'admin';

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function add(): ResponseInterface
    {
        $title       = (string)$this->request->getVar('title');
        $description = (string)$this->request->getVar('description');

        $count = $this->modules->todo->getCountByType($this->type, Storage::$admin->id);
        if($count > $this->modules->config->backendConfig->get('maxTodoItem', 20))
            throw new ClientBadRequest("Errors.Reach Maximum todo.");

        return $this->sendDone(['id' => $this->modules->todo->addTodo(
            $this->type, Storage::$admin->id, strip_tags($title), strip_tags($description)),
        ]);
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws ServerInternalError
     */
    public function remove(): ResponseInterface
    {
        $record = $this->getRecord();
        $this->modules->todo->remove($record->id);
        return $this->sendSuccess("General.Delete Successfully.", data: TRUE);
    }

    /**
     * @return TodosEntity|null
     * @throws ClientBadRequest
     */
    private function getRecord(): ?TodosEntity
    {
        if(!$id = (int)$this->request->getVar('id'))
            throw new ClientBadRequest("Errors.require id");
        elseif(!$record = $this->models->todos->getById($id))
            throw new ClientBadRequest("Errors.Invalid record");
        elseif($record->type !== $this->type || $record->typeId !== Storage::$admin->id)
            throw new ClientBadRequest("Errors.Invalid id");
        return $record;

    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ServerInternalError
     * @throws ReflectionException
     */
    public function complete(): ResponseInterface
    {
        $record = $this->getRecord();
        $this->modules->todo->complete($record->id, (int)$this->request->getVar('completed'));
        return $this->sendSuccess("General.Update Successfully.", data: TRUE);
    }

    /**
     * @throws ReflectionException
     * @throws DbQueryError
     * @throws ServerInternalError
     */
    public function batchComplete(): ResponseInterface
    {
        $ids         = (array)$this->request->getVar('ids');
        $isCompleted = (int)$this->request->getVar('completed') === 1 ? 1 : 0;
        $this->modules->todo->batchCompleteByType($this->type, Storage::$admin->id, $ids, $isCompleted);
        return $this->sendSuccess("General.Update Successfully.", data: TRUE);
    }

    /**
     * @return ResponseInterface
     * @throws ServerInternalError
     */
    public function batchDelete(): ResponseInterface
    {
        $ids = (array)$this->request->getVar('ids');
        $this->modules->todo->batchDeleteByType($this->type, Storage::$admin->id, $ids);
        return $this->sendSuccess("General.Delete Successfully.", data: TRUE);
    }

    /**
     * @return ResponseInterface
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws ServerInternalError
     */
    public function updateInfo(): ResponseInterface
    {
        $record = $this->getRecord();
        $this->modules->todo->updateTodo($record->id, (string)$this->request->getVar('title'));
        return $this->sendSuccess("General.Update Successfully.", data: TRUE);
    }

    public function listing(): ResponseInterface
    {
        $completed = NULL;
        $keyword   = (string)$this->request->getVar('keyword');
        $orderBy   = $this->request->getVar('orderBy') === 'desc' ? 'desc' : 'asc';
        if($status = (string)$this->request->getVar('status'))
            $completed = $status === 'done' ? 1 : 0;
        return $this->sendDone([
            'data'       => $this->modules->todo->getListByType($this->type, Storage::$admin->id, $completed, $keyword, $orderBy),
            'pagination' => $this->modules->pagination->data(),
        ]);
    }
}