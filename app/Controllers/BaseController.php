<?php
declare(strict_types=1);

namespace App\Controllers;

use Predis\Client;
use Config\Services;
use App\Modules\Models;
use App\Modules\Modules;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Tool\WsResourceController;

/**
 * Class BaseController
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 * For security be sure to declare any new methods as protected or private.
 * @property-read  Models  $models
 * @property-read  Modules $modules
 * @property-read  Client  $redis
 */
abstract class BaseController extends WsResourceController
{
    public function respond($data = NULL, ?int $status = NULL, string $message = ''): ResponseInterface
    {
        return parent::respond($data, $status, $message);
    }

    /**
     * @param $name
     * @return Models|Modules|Client|null
     */
    final public function __get($name)
    {
        return match ($name) {
            'models'  => Services::models(),
            'modules' => Services::modules(),
            'redis'   => Services::redis(),
            default   => NULL,
        };
    }

    /**
     * @return int
     */
    final public function getCurrentPage(): int
    {
        return (int)($this->request->getVar('page') ?: 1);
    }

    /**
     * @return int
     */
    final public function getPageSizeLimit(): int
    {
        return (int)($this->request->getVar('limit') ?: Services::pager()->getPerPage());
    }
}
