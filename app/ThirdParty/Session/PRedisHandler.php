<?php
declare(strict_types=1);

/**
 * This file is part of CodeIgniter 4 framework.
 * (c) CodeIgniter Foundation <admin@codeigniter.com>
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\ThirdParty\Session;

use Throwable;
use Config\Services;
use Wellous\Ci4Component\Session\WsSessPRedisHandler;

/**
 * Session handler using Redis for persistence
 */
class PRedisHandler extends WsSessPRedisHandler
{
    /**
     * Re-initialize existing session, or creates a new one.
     * @param string $path The path where to store/retrieve the session
     * @param string $name The session name
     */
    public function open(string $path, string $name): bool
    {
        try {
            $this->redis = Services::redis();
            return TRUE;
        } catch(Throwable) {
            return FALSE;
        }
    }
}
