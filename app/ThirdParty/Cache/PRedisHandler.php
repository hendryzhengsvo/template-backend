<?php
declare(strict_types=1);

namespace App\ThirdParty\Cache;

/**
 * This file is third party for CodeIgniter 4 framework.
 */

use Throwable;
use Config\Services;
use CodeIgniter\Exceptions\CriticalError;
use Wellous\Ci4Component\Cache\WsCachePRedisHandler;

class PRedisHandler extends WsCachePRedisHandler
{
    /**
     * {@inheritDoc}
     */
    public function initialize(): void
    {
        try {
            $this->redis = Services::redis();
        } catch(Throwable $e) {
            throw new CriticalError('Cache: RedisException occurred with message (' . $e->getMessage() . ').');
        }
    }
}
