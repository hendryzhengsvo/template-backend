<?php
declare(strict_types=1);

namespace App\Modules;

use App\Traits\ModuleTrait;

/**
 * ModuleTrait Base
 */
abstract class Base
{
    use ModuleTrait;

    /**
     * @var Modules
     */
    protected Modules $modules;
    /**
     * @var Models
     */
    protected Models $models;
    /**
     * @var Base|null
     */
    protected Base|null $parent;

    public function __construct(Modules $modules = NULL, ?Base $parent = NULL)
    {
        $this->modules = $modules;
        $this->models  = $modules->models;
        $this->parent  = $parent;
    }
}
