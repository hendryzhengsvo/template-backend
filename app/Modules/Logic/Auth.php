<?php
declare(strict_types=1);

namespace App\Modules\Logic;

use Config\Services;
use App\Modules\Base;
use App\Modules\Logic\Auth\OAuth;
use App\Modules\Logic\Auth\Admin;
use RobThree\Auth\TwoFactorAuthException;

/**
 * Class Member
 * @package App\Modules\Logic
 * @property Admin $admin
 * @property OAuth $oAuth
 */
class Auth extends Base
{
    /**
     * @var string
     */
    protected string $componentPath = 'App\Modules\Logic\Auth';

    /**
     * @param $email
     * @return string
     */
    public function generateKey($email): string
    {
        $action = "email-verify:$email";
        return substr(sha1($email), 0, -6) . Services::twoFactor()::getCode($action);
    }

    /**
     * @param $email
     * @param $key
     * @return bool
     * @throws TwoFactorAuthException
     */
    public function verifyKey($email, $key): bool
    {
        [$key, $code] = str_split($key, 34);
        $action = "email-verify:$email";
        return substr(sha1($email), 0, -6) === $key && Services::twoFactor()::verify($action, $code, 144);
    }
}
