<?php
declare(strict_types=1);

namespace App\Modules\Logic\Auth\OAuth;

use App\Libraries\Utilities;
use App\Modules\Base as ModuleBase;

/**
 * Class Base
 * @package Module\OAuth
 */
abstract class Base extends ModuleBase
{
    /**
     * @var array
     */
    protected array $config = [];

    /**
     * Config Name
     */
    protected string $platform = '';
    /**
     * @var string $redirectUrl Redirect URL for OAuth
     */
    protected string $redirectUrl;

    /**
     * Base constructor.
     */
    public function __construct($modules = NULL, ?ModuleBase $parent = NULL)
    {
        parent::__construct($modules, $parent);
        $this->config      = config('OAuth')->{$this->platform};
        $this->redirectUrl = Utilities::getFullUrl();
    }
}