<?php
declare(strict_types=1);

namespace App\Modules\Logic\Auth\OAuth;

class AdminFacebook extends Facebook
{
    protected string $panel = 'admin';
}