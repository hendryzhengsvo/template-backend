<?php
declare(strict_types=1);

namespace App\Modules\Logic\Auth\OAuth;

class AdminGoogle extends Google
{
    protected string $panel = 'admin';
}