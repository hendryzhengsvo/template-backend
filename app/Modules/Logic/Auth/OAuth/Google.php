<?php
declare(strict_types=1);

namespace App\Modules\Logic\Auth\OAuth;

use App\Libraries\Rpc;
use ReflectionException;
use App\Libraries\Utilities;
use App\Entities\AdminEntity;
use CodeIgniter\Events\Events;
use GuzzleHttp\Exception\GuzzleException;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

/**
 * Class Google
 */
class Google extends Base
{
    protected string $panel = '';

    /**
     * Config Name
     */
    protected string $platform = 'google';

    /**
     * @param $panelUserId
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function addConnect($panelUserId): void
    {
        if(!$this->models->oauthConnect->getByParty($this->panel, $this->platform)) {
            $this->models->oauthConnect->create($panelUserId, $this->panel, $this->platform);
        }
    }

    /**
     * connect facebook
     * @param array $oauthUser
     * @return AdminEntity|int|false
     * @throws DbQueryError
     * @throws ReflectionException
     */
    final public function connect(array $oauthUser): AdminEntity|int|false
    {
        $panelUserId = $this->models->{$this->panel}->insert([
            'name'          => $oauthUser['name'] ?? '',
            'email'         => $oauthUser['email'] ?? '',
            'avatar'        => $oauthUser['picture'] ?? '',
            'home_route'    => json_encode(['name' => 'Admin/Home']),
            'date_of_birth' => NULL,
            'verify'        => 1,
        ]);
        $this->models->oauthConnect->create($panelUserId, $this->panel, $this->platform);
        Events::trigger("{$this->panel}_register", (int)$panelUserId);
        Events::trigger($this->platform . "_connect_{$this->panel}_register", (int)$panelUserId);

        return $this->models->{$this->panel}->getById($panelUserId);
    }

    /**
     * login with facebook
     * @param string $code
     * @return AdminEntity|int|false
     * @throws GuzzleException
     * @throws ServerInternalError
     */
    final public function login(string $code): AdminEntity|int|false
    {
        $this->parseCode($code);
        if($oc = $this->models->oauthConnect->getByParty($this->panel, $this->platform)) {
            return $this->modules->{$this->panel}->getByEmail($oc->roleid);
        } else return FALSE;
    }

    /**
     * get facebook profile
     * @param string $code
     * @return array
     * @throws ServerInternalError
     */
    public function parseCode(string $code): array
    {
        $token = $this->getAccessToken($this->redirectUrl, $code);
        if(!isset($token['id_token']))
            throw new ServerInternalError();
        elseif(!$oauthUser = $this->getUserInfo($token['id_token']))
            throw new ServerInternalError();
        return $oauthUser;
    }

    /**
     * @param $redirectUri
     * @param $code
     * @return array|boolean
     */
    final public function getAccessToken($redirectUri, $code): bool|array
    {
        return Rpc::post('/token', [
            'form_params' => [
                'client_id'     => $this->config['client_id'],
                'client_secret' => $this->config['client_secret'],
                'redirect_uri'  => $redirectUri,
                'grant_type'    => 'authorization_code',
                'code'          => $code,
            ],
        ]);
    }

    /**
     * @param string $idToken
     * @return array
     */
    final public function getUserInfo(string $idToken): array
    {
        $data = explode('.', $idToken);
        foreach($data as &$item)
            $item = base64_decode($item);
        $userInfo = json_decode($data[1], TRUE);
        $result   = [];
        if(!empty($userInfo['sub']))
            $result['id'] = $userInfo['sub'];
        if(!empty($userInfo['name']))
            $result['name'] = $userInfo['name'];
        if(!empty($userInfo['email']))
            $result['email'] = $userInfo['email'];
        if(!empty($userInfo['picture']))
            $result['picture'] = $userInfo['picture'];
        if(!empty($userInfo['email_verified']))
            $result['verify'] = $userInfo['email_verified'];
        if(!empty($userInfo['ttl']))
            $result['ttl'] = $userInfo['exp'];
        return $result;
    }

    /**
     * @param string $redirectUri
     * @param string $prompt
     * @param string $state
     * @return string
     */
    final public function getOAuthUrl(string $redirectUri, string $state, string $prompt = 'none'): string
    {
        $params = [
            'client_id'              => $this->config['client_id'],
            'response_type'          => 'code',
            'redirect_uri'           => $redirectUri,
            'prompt'                 => $prompt,
            'include_granted_scopes' => 'true',
            'nonce'                  => Utilities::genUuidV4(),
        ];
        if(!empty($this->config['scopes']))
            $params['scope'] = implode(' ', $this->config['scopes']);
        if(!empty($state))
            $params['state'] = $state;
        return "{$this->config['endpoint']}?" . http_build_query($params);
    }
}