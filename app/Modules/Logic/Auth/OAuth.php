<?php
declare(strict_types=1);

namespace App\Modules\Logic\Auth;

use App\Modules\Base;
use App\Modules\Logic\Auth\OAuth\Google;
use App\Modules\Logic\Auth\OAuth\Facebook;
use App\Modules\Logic\Auth\OAuth\AdminGoogle;
use App\Modules\Logic\Auth\OAuth\AdminFacebook;

/**
 * @property AdminFacebook $adminFacebook
 * @property AdminGoogle   $adminGoogle
 * @property Google        $google
 * @property Facebook      $facebook
 */
class OAuth extends Base
{
    protected string $componentPath = 'App\Modules\Logic\Auth\OAuth';
}