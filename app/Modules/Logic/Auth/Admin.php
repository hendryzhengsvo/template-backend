<?php
declare(strict_types=1);

namespace App\Modules\Logic\Auth;

use Config\Services;
use App\Modules\Base;
use App\Modules\Storage;
use ReflectionException;
use App\Libraries\Utilities;
use App\Entities\AdminEntity;
use App\Libraries\DateTimeMethod;
use lbuchs\WebAuthn\WebAuthnException;
use App\Entities\AdminBiometricsEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use lbuchs\WebAuthn\Attestation\AttestationObject;
use Wellous\Ci4Component\Exceptions\ClientBadRequest;
use Wellous\Ci4Component\Exceptions\ClientNotAcceptable;
use function hash;
use function is_object;
use function property_exists;
use function openssl_pkey_get_public;

class Admin extends Base
{
    /**
     * @param string $token
     * @return AdminEntity|null
     * @throws ClientNotAcceptable
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function loginByToken(string $token): ?AdminEntity
    {
        if(($admin = Services::redis()->getUnserialize($token))) {
            $admin = (new AdminEntity($admin))->syncOriginal();
            $this->modules->visitor->loginVisitor($admin->id);
            Storage::$admin = $admin;
            Services::redis()->del($token);
            return $admin;
        } else
            throw new ClientNotAcceptable('Invalid token');
    }

    /**
     * @param $email
     * @param $password
     * @return AdminEntity|null
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function login($email, $password): ?AdminEntity
    {
        if($admin = $this->models->admin->getByLogin($email)) {
            //only status is active then allow login
            if($admin->password !== sha1($admin->email . $password))
                throw new ClientBadRequest('', 'Exceptions.Username/Password is wrong');

            if($admin->status !== 1)
                throw new ClientBadRequest('', 'Exceptions.Status inactive');

            $this->modules->visitor->loginVisitor($admin->id);
            return $admin;
        }
        return NULL;
    }

    /**
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function logout(): bool
    {
        return $this->modules->visitor->logoutVisitor();
    }

    /**
     * @param int $adminId
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function logLogin(int $adminId): void
    {
        $ua = Services::request()->getUserAgent();
        $this->models->adminLogin->insert([
            'admin_id'  => $adminId,
            'login_ip'  => Utilities::getClientIp(),
            'useragent' => $ua->getAgentString() ?? 'Unknown',
            'browser'   => $ua->getBrowser() ?? 'Unknown',
            'platform'  => $ua->getPlatform() ?? 'Unknown',
            'source'    => $ua->getMobile() ?? 'Unknown',
        ]);
    }

    /**
     * @param int   $adminId
     * @param array $regCredential
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws WebAuthnException
     */
    public function addBiometric(int $adminId, array $regCredential): bool
    {
        if(!isset($regCredential['response']['attestationObject']) || !isset($regCredential['response']['clientDataJSON']))
            return FALSE;
        $attestationObject              = new AttestationObject(base64_decode($regCredential['response']['attestationObject']), ['none', 'android-key', 'android-safetynet', 'apple', 'fido-u2f', 'packed', 'tpm']);
        $regCredential['publickey_pem'] = $attestationObject->getAuthenticatorData()->getPublicKeyPem();
        $credentialId                   = $regCredential['id'];
        if(strlen($credentialId) > 128)
            $credentialId = substr($credentialId, 0, 128);
        $this->models->adminBiometrics->insert(new AdminBiometricsEntity([
            'admin_id'      => $adminId,
            'credential_id' => $credentialId,
            'credential'    => $regCredential,
            'created_at'    => DateTimeMethod::datetime(),
        ]));
        return TRUE;
    }

    /**
     * @param int   $adminId
     * @param array $authCredential
     * @return bool
     */
    public function removeBiometric(int $adminId, array $authCredential): bool
    {
        if(!isset($authCredential['response']['clientDataJSON']) || !isset($authCredential['response']['userHandle']) || !isset($authCredential['response']['authenticatorData']) || !isset($authCredential['response']['signature']) || !isset($authCredential['id']))
            return FALSE;
        $credentialId = $authCredential['id'];
        if(strlen($credentialId) > 128)
            $credentialId = substr($credentialId, 0, 128);
        return $this->models->adminBiometrics->deleteByCredentialId($adminId, $credentialId);
    }

    /**
     * @param array $authCredential
     * @return AdminEntity|null
     * @throws DbQueryError
     * @throws ReflectionException
     * @throws WebAuthnException
     */
    public function loginByBiometric(array $authCredential): ?AdminEntity
    {
        if(!isset($authCredential['response']['clientDataJSON']) || !isset($authCredential['response']['userHandle']) || !isset($authCredential['response']['authenticatorData']) || !isset($authCredential['response']['signature']) || !isset($authCredential['id']))
            return NULL;
        $authCredential['response']['clientDataJSON'] = base64_decode($authCredential['response']['clientDataJSON']);
        $clientData                                   = json_decode($authCredential['response']['clientDataJSON']);

        if(!is_object($clientData))
            throw new WebAuthnException('invalid client data', WebAuthnException::INVALID_DATA);

        $originHost = parse_url(Services::request()->header('origin')->getValue(), PHP_URL_HOST);
        $authHost   = parse_url($clientData->origin, PHP_URL_HOST);
        if($originHost !== $authHost)
            throw new WebAuthnException('invalid origin', WebAuthnException::INVALID_ORIGIN);

        if(!property_exists($clientData, 'type') || $clientData->type !== 'webauthn.get')
            throw new WebAuthnException('invalid type', WebAuthnException::INVALID_TYPE);

        $credentialId = $authCredential['id'];
        if(strlen($credentialId) > 128)
            $credentialId = substr($credentialId, 0, 128);
        if($biometrics = $this->models->adminBiometrics->getByCredentialId($credentialId)) {
            $clientDataHash = hash('sha256', $authCredential['response']['clientDataJSON'], TRUE);
            $dataToVerify   = base64_decode($authCredential['response']['authenticatorData']);
            $dataToVerify   .= $clientDataHash;

            $publicKey = openssl_pkey_get_public($biometrics->credential['publickey_pem']);
            if($publicKey === FALSE)
                throw new WebAuthnException('public key invalid', WebAuthnException::INVALID_PUBLIC_KEY);

            if(openssl_verify($dataToVerify, base64_decode($authCredential['response']['signature']), $publicKey, OPENSSL_ALGO_SHA256) !== 1)
                throw new WebAuthnException('invalid signature', WebAuthnException::INVALID_SIGNATURE);

            $admin = $this->models->admin->getById($biometrics->adminId);
            $this->modules->visitor->loginVisitor($admin->id);
            return $admin;
        }
        return NULL;
    }

    /**
     * @param AdminEntity $admin
     * @param string      $token
     * @return string
     */
    public function generateToken(AdminEntity $admin, string $token = ''): string
    {
        if(!$token)
            $token = sha1(Utilities::genUuidV4());
        Services::redis()->setSerialize($token, $admin->toRawArray(), 3600);
        return $token;
    }
}
