<?php
declare(strict_types=1);

namespace App\Modules\Logic;

use App\Modules\Base;
use ReflectionException;
use App\Entities\TodosEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;


class Todo extends Base
{
    /**
     * @param string $type
     * @param int    $typeId
     * @param string $title
     * @param string $description
     * @return int
     * @throws ReflectionException
     * @throws DbQueryError
     */
    public function addTodo(string $type, int $typeId, string $title, string $description = ''): int
    {
        $entity              = new TodosEntity();
        $entity->type        = $type;
        $entity->typeId      = $typeId;
        $entity->title       = $title;
        $entity->description = $description;
        $entity->createdAt   = date("Y-m-d H:i:s");
        $entity->isCompleted = 0;
        return (int)$this->models->todos->insert($entity);
    }

    /**
     * @param int    $id
     * @param string $title
     * @param string $description
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function updateTodo(int $id, string $title, string $description = ''): bool
    {
        $data = ['title' => $title];
        if($description) $data['description'] = $description;
        return $this->models->todos->update($id, $data);
    }

    /**
     * @param int $id
     * @param int $completed
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function complete(int $id, int $completed = 1): bool
    {
        return $this->models->todos->update($id, ['is_completed' => $completed > 0 ? 1 : 0]);
    }


    /**
     * @param int $id
     * @return bool
     */
    public function remove(int $id): bool
    {
        return (bool)$this->models->todos->delete($id);
    }

    /**
     * @param string   $type
     * @param int      $typeId
     * @param int|null $completed
     * @param string   $keyword
     * @param string   $orderBy
     * @return array|int|string
     */
    public function getListByType(string $type, int $typeId, int|null $completed, string $keyword = '', string $orderBy = 'asc'): int|array|string
    {
        $data = $this->models->todos->getListByType($type, $typeId, $completed, $keyword, $orderBy, $this->modules->pagination->getPageSize(), $this->modules->pagination->getOffset());
        $this->modules->pagination->init($this->models->todos->getListByType($type, $typeId, $completed, $keyword, $orderBy, count: TRUE));
        return $data;
    }

    /**
     * @param string $type
     * @param int    $typeId
     * @return int
     */
    public function getCountByType(string $type, int $typeId): int
    {
        return (int)$this->models->todos->where(['type' => $type, 'type_id' => $typeId])
                        ->selectCount('*', 'cnt')->first()['cnt'] ?? 0;
    }

    /**
     * @param string $type
     * @param int    $typeId
     * @param array  $ids
     * @param int    $isCompleted
     * @return false|int
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function batchCompleteByType(string $type, int $typeId, array $ids, int $isCompleted): bool|int
    {
        return $this->models->todos->batchCompleteByType($type, $typeId, $ids, $isCompleted);
    }

    /**
     * @param string $type
     * @param int    $typeId
     * @param array  $ids
     * @return bool|int
     */
    public function batchDeleteByType(string $type, int $typeId, array $ids): bool|int
    {
        return $this->models->todos->batchDeleteByType($type, $typeId, $ids);
    }


}