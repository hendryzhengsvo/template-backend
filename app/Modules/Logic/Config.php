<?php
declare(strict_types=1);

namespace App\Modules\Logic;

use App\Modules\Base;
use App\Modules\Logic\Config\BackendConfig;
use App\Modules\Logic\Config\FrontendConfig;

/**
 * @property BackendConfig  $backendConfig
 * @property FrontendConfig $frontendConfig
 */
class Config extends Base
{
    protected string $componentPath = 'App\Modules\Logic\Config';
}