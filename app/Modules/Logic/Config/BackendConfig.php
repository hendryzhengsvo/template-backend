<?php
declare(strict_types=1);

namespace App\Modules\Logic\Config;

use Config\Services;
use App\Modules\Base;
use App\Modules\Modules;
use ReflectionException;
use App\Entities\ConfigEntity;
use CodeIgniter\Events\Events;
use Wellous\Ci4Component\Exceptions\DbQueryError;

class BackendConfig extends Base
{
    /**
     * Variable to store the time-to-live value for the cache entry.
     * @var int
     */
    protected int $ttl = 60;
    /**
     * @var array
     */
    private array $data = [];
    /**
     * Variable to store the name of the cache.
     * @var string
     */
    private string $cacheName;
    private string $platform;

    public function __construct(?Modules $modules = NULL, Base|null $parent = NULL)
    {
        parent::__construct($modules, $parent);
        $this->platform  = Services::request()->getHeaderLine('platform');
        $this->cacheName = "config:backend:$this->platform:all:" . (time() - (time() % $this->ttl));
        Events::on('afterDbRollback', function()
        {
            $this->reload();
        });
    }

    /**
     * Clears the current data and reloads it from the cache.
     * This method clears the current data by resetting the data property to an empty array
     * and deletes the cache entry associated with the cache name using the Redis del command.
     * After calling this method, the data will be reloaded from the cache when needed.
     * @return void
     */
    public function reload(): void
    {
        $this->data = [];
        Services::redis()->del($this->cacheName);
    }

    /**
     * Retrieves the value associated with a given key from the storage.
     * @param string $key     The key to retrieve the value for.
     * @param mixed  $default The default value to return if the key does not exist in the storage.
     * @return mixed The value associated with the given key, or the default value if the key does not exist.
     */
    public function get(string $key, mixed $default = NULL): mixed
    {
        $value = $this->retrieve($key);
        return $value ?? $default;
    }

    /**
     * Retrieve the value associated with the given key from the cache or database.
     * @param string $key The key to retrieve the value for.
     * @return mixed The retrieved value.
     */
    private function retrieve(string $key): mixed
    {
        if(!isset($this->data[$key])) {
            $value = Services::redis()->hget($this->cacheName, $key);
            if($value === NULL) {
                if($config = $this->raw($key)) {
                    $value = $config->value;
                    Services::redis()->hset($this->cacheName, $key, serialize($value));
                    Services::redis()->expire($this->cacheName, $this->ttl);
                }
            } else
                $value = unserialize($value);
            $this->data[$key] = $value;
        }
        return $this->data[$key];
    }

    /**
     * Retrieves the value associated with a given key from the storage.
     * @param string $key        The key to retrieve the value for.
     * @return ConfigEntity|null The value associated with the given key, or the default value if the key does not
     *                           exist.
     */
    public function raw(string $key): ?ConfigEntity
    {
        return $this->modules->models->config->getByPlatformKey($this->platform, $key);
    }

    /**
     * Sets the value for a given key.
     * @param string $key   The key to set the value for.
     * @param mixed  $value The value to be set.
     * @return bool True on success, false on failure.
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function set(string $key, mixed $value): bool
    {
        /** @var ConfigEntity $config */
        if(!($config = $this->raw($key)))
            $config = new ConfigEntity([
                'key'      => $key,
                'platform' => $this->platform,
            ]);
        $config->value = $value;
        if(($success = $this->models->config->save($config))) {
            Services::redis()->hset($this->cacheName, $key, serialize($config->value));
            Services::redis()->expire($this->cacheName, $this->ttl);
        }
        return $success;
    }
}
