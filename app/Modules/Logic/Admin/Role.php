<?php
declare(strict_types=1);

namespace App\Modules\Logic\Admin;

use App\Modules\Base;
use App\Modules\Storage;
use App\Entities\AdminMenuEntity;
use App\Entities\AdminRolesEntity;
use Wellous\Ci4Component\Exceptions\ClientForbidden;

class Role extends Base
{
    /**
     * @var string
     */
    protected string $componentPath = 'App\Modules\Logic\Admin\Role';

    /**
     * @param int  $roleId
     * @param bool $getDetail
     * @return AdminRolesEntity|null
     */
    public function getRole(int $roleId, bool $getDetail = FALSE): ?AdminRolesEntity
    {
        if($role = $this->models->adminRoles->getById($roleId)) {
            if($getDetail) {
                $role->actions = $this->getActions($role->id);
                $role->menu    = $this->getMenu($role->id);
            }
        }

        return $role ?? NULL;

    }

    /**
     * @param int $roleId
     * @return array
     */
    public function getActions(int $roleId): array
    {
        $data = [];
        if($actionIds = $this->models->adminRoleActions->getActionIdsByRoleId($roleId))
            $data = $this->models->adminActions->getActionByIds($actionIds);
        return $data;
    }

    public function getMenu(int $roleId): array
    {
        $data    = [];
        $menuIds = $this->models->adminRoleMenu->getMenuIdsByRoleId($roleId);
        if($menuIds && ($menuItems = $this->models->adminMenu->getMenuItemsByIds($menuIds)))
            $data = $this->buildMenuItem($menuItems);
        return $data;
    }

    /**
     * @param array $elements
     * @param int   $parentId
     * @return array
     */
    private function buildMenuItem(array $elements, int $parentId = 0): array
    {
        $branch = [];
        /** @var AdminMenuEntity $element */
        foreach($elements as $element)
            if($element->status > 0) {
                if($element->parentId === $parentId) {
                    $branch[$element->id] = $element->outputItem();
                    if(($children = $this->buildMenuItem($elements, $element->id)))
                        $branch[$element->id]['children'] = $children;
                }
            }
        return array_values($branch);
    }

    /**
     * @param string $action
     * @return bool
     * @throws ClientForbidden
     */
    public function checkAction(string $action): bool
    {
        $this->initActions();
        if($action && in_array($action, Storage::$adminActions, TRUE))
            return TRUE;
        throw new ClientForbidden("Permission Deny", "Permission Deny");
    }

    public function initActions(): void
    {
        if(!Storage::$adminActions) {
            Storage::$adminActions = $this->getActions(Storage::$admin->roleId);
        }
    }
}
