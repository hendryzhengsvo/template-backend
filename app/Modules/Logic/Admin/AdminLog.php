<?php
declare(strict_types=1);

namespace App\Modules\Logic\Admin;

use App\Modules\Base;
use ReflectionException;
use App\Entities\AdminLogEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;

class AdminLog extends Base
{
    /**
     * @var string
     */
    protected string $componentPath = 'App\Modules\Logic\Admin\Member';
    private array    $adminList     = [];

    /**
     * @param string $table
     * @param int    $refId
     * @return array
     */
    public function getLastUpdatedBy(string $table, int $refId): array
    {
        $result = $this->models->adminLog->getLastUpdatedByRef($table, $refId);
        if(!$result)
            return [];
        if(!isset($this->adminList[$result->adminId]))
            $this->adminList[$result->adminId] = $this->models->admin->getById($result->adminId);

        $admin           = $this->adminList[$result->adminId];
        $result          = $result->toArray();
        $result['admin'] = [
            'id'       => $admin->id,
            'name'     => $admin->name,
            'nickname' => $admin->nickname,
            'email'    => $admin->email,
        ];
        return $result;
    }

    /**
     * @param int    $adminId
     * @param int    $refId
     * @param string $table
     * @param int    $visitorId
     * @param string $action
     * @param array  $params
     * @return int
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function addLog(int $adminId, int $refId, string $table, int $visitorId, string $action, array $params): int
    {
        if(isset($params['page'])) {
            unset($params['page']);
        }
        if(isset($params['filter'])) {
            unset($params['filter']);
        }
        if(isset($params['search'])) {
            unset($params['search']);
        }
        if(isset($params['page_size'])) {
            unset($params['page_size']);
        }
        return $this->models->adminLog->create($adminId, $refId, $table, $visitorId, $action, $params);
    }

    /**
     * @param $filter
     * @return array
     */
    public function listing($filter): array
    {
        $data = $this->models->adminLog->getAdminListing($filter, $this->modules->pagination->getPageSize(), $this->modules->pagination->getOffset());
        $this->modules->pagination->init($this->models->adminLog->getAdminListingTotal($filter));
        $adminListing = $this->models->admin->getAllAdminListing();

        $result = [];
        /**
         * @var $item AdminLogEntity
         */
        foreach($data as $item) {

            $admin    = $adminListing[$item->adminId];
            $result[] = [
                'id'        => $item->id,
                'admin'     => [
                    'id'       => $admin['id'],
                    'name'     => $admin['name'],
                    'nickname' => $admin['nickname'],
                    'email'    => $admin['email'],
                ],
                'refId'     => $item->refId,
                'table'     => $item->refTable,
                'visitorId' => $item->visitorId,
                'action'    => $item->action,
                'params'    => $item->param,
                'createdAt' => $item->createdAt,
            ];
        }
        $filterListing = $this->getFilterListing();
        return [
            'listing'            => $result,
            'admin_listing'      => array_values($adminListing),
            'action_listing'     => $filterListing['action'],
            'references_listing' => $filterListing['references'],
        ];
    }

    private function getFilterListing(): array
    {
        $actions    = [];
        $references = [];
        foreach(ADMIN_ACTIONS as $cat => $val) {
            $references[] = $cat;
            $actions[]    = [
                'label' => $cat,
                'items' => [],
            ];
            foreach($val as $item) {
                $actions[count($actions) - 1]['items'][] = $item;
            }
        }
        return [
            'action'     => $actions,
            'references' => $references,
        ];
    }
}