<?php
declare(strict_types=1);

namespace App\Modules\Logic;

use Config\Services;
use App\Modules\Base;
use App\Modules\Storage;
use ReflectionException;
use App\Libraries\Utilities;
use App\Entities\VisitorEntity;
use App\Libraries\DateTimeMethod;
use Wellous\Ci4Component\Exceptions\DbQueryError;

class Visitor extends Base
{
    /**
     * @param string $visitorId
     * @param string $platform
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function visit(string $visitorId, string $platform = 'member'): bool
    {
        if(!($visitor = $this->getVisitor($visitorId, $platform))) {
            $visitor     = new VisitorEntity([
                'code'         => Utilities::genCode(64),
                'platform'     => $platform,
                'login_id'     => 0,
                'status'       => 1,
                'logged_at'    => NULL,
                'lastseen_at'  => DateTimeMethod::timestamp(),
                'firstseen_at' => DateTimeMethod::timestamp(),
            ]);
            $visitor->id = $this->models->visitor->insert($visitor);
        }

        return (bool)$this->setVisitor($visitor);
    }

    /**
     * @param string $code
     * @param string $platform
     * @return VisitorEntity|null
     */
    private function getVisitor(string $code, string $platform): ?VisitorEntity
    {
        if(empty($code))
            return NULL;
        elseif(!($visitor = Services::redis()->getUnserialize("wm:visitor:$platform:$code")))
            return $this->models->visitor->getByCode($code);
        else
            return (new VisitorEntity($visitor))->syncOriginal();
    }

    /**
     * @param VisitorEntity $visitor
     * @return bool|int|string
     * @throws DbQueryError
     * @throws ReflectionException
     */
    private function setVisitor(VisitorEntity $visitor): bool|int|string
    {
        $visitor->lastseenAt = DateTimeMethod::timestamp();
        Services::redis()->setSerialize("wm:visitor:$visitor->platform:$visitor->code", $visitor->toRawArray(), 60);
        Storage::$visitor = $visitor;
        if(empty($visitor->id)) {
            $visitor->id = NULL;
            return $visitor->id = $this->models->visitor->insert($visitor);
        } elseif($visitor->hasChanged())
            return $this->models->visitor->save($visitor);
        else
            return TRUE;
    }

    /**
     * @param int $loginId
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function loginVisitor(int $loginId): bool
    {
        if(!Storage::$visitor || Storage::$visitor->status !== 1)
            return FALSE;
        Storage::$visitor->loginId  = $loginId;
        Storage::$visitor->loggedAt = DateTimeMethod::timestamp();
        return $this->setVisitor(Storage::$visitor);
    }

    /**
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function logoutVisitor(): bool
    {
        if(!Storage::$visitor)
            return FALSE;
        Storage::$visitor->loginId  = 0;
        Storage::$visitor->loggedAt = NULL;
        return (bool)$this->setVisitor(Storage::$visitor);
    }
}