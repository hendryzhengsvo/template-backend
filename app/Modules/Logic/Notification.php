<?php
declare(strict_types=1);

namespace App\Modules\Logic;

use App\Modules\Base;
use App\Modules\Storage;
use ReflectionException;
use App\Libraries\DateTimeMethod;
use App\Entities\AdminWebPushEntity;
use App\Entities\AdminNotificationEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;

class Notification extends Base
{
    /**
     * @param int $adminId
     * @return void
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function addWelcomeNotice(int $adminId): void
    {
        $this->models->adminNotification->create(
            $adminId,
            'misc',
            'General.Welcome to Admin Panel Website',
            'General.Admin Panel welcome you, there are a lots of rewards waiting you to redeem.'
        );
    }

    /**
     * @param $category
     * @param $limit
     * @param $offset
     * @return array
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function getCategorizeNotification($category, $limit, $offset): array
    {
        $data = [];
        $list = $this->models->adminNotification->getByCategory(Storage::$admin->id, $category, $limit, $offset);
        if($list) {
            /** @var AdminNotificationEntity $row */
            foreach($list as $row) {
                $data[] = [
                    'category' => $row->category,
                    'created'  => $row->createdAt,
                    'title'    => lang($row->title),
                    'content'  => $this->translateParams($row->content, $row->params),
                    'status'   => $row->status,
                    'params'   => $row->params,
                ];
            }
        }
        $this->models->adminNotification->markAsRead(Storage::$admin->id, $category);
        return $data;
    }

    /**
     * @param string $contentData
     * @param array  $params
     * @return string
     */
    public function translateParams(string $contentData, array $params): string
    {
        //hardcode translate status word because it is inside params
        if($params) {
            if(!empty($params['statusWord'])) {
                $params['statusWord'] = lang('General.' . $params['statusWord']);
            }
            $content = lang($contentData, $params);
        } else {
            $content = lang($contentData);
        }
        return $content;
    }

    /**
     * @param int|string $adminId
     * @param array      $subscription
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function subscribeWebPush(int|string $adminId, array $subscription): void
    {
        /** @var AdminWebPushEntity $entity */
        $entity = $this->models->adminWebPush->get($subscription['endpoint']);
        if(!$entity || $entity->adminId !== (int)$adminId) {
            if($entity)
                $this->models->adminWebPush->delete($entity->id);
            $entity               = new AdminWebPushEntity();
            $entity->adminId      = $adminId;
            $entity->subscription = $subscription;
            $entity->status       = 1;
            $entity->createdAt    = $entity->updatedAt = DateTimeMethod::datetime();
            $this->models->adminWebPush->insert($entity);
        }
    }

    /**
     * @param int $adminId
     */
    public function unsubscribeWebPush(int $adminId): void
    {
        $this->models->adminWebPush->deleteByAdminId($adminId);
    }
}