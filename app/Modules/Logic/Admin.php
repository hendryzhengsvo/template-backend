<?php
declare(strict_types=1);

namespace App\Modules\Logic;

use Config\Services;
use App\Modules\Base;
use App\Modules\Storage;
use ReflectionException;
use App\Libraries\Provider;
use App\Libraries\Utilities;
use App\Entities\AdminEntity;
use CodeIgniter\Events\Events;
use App\Libraries\DateTimeMethod;
use App\Modules\Logic\Admin\Role;
use App\Modules\Logic\Admin\AdminLog;
use CodeIgniter\HTTP\Files\UploadedFile;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ClientBadRequest;

/**
 * Modules Modules
 * @property AdminLog $adminLog
 * @property Role     $role
 */
class Admin extends Base
{
    /**
     * @var string
     */
    protected string $componentPath = 'App\Modules\Logic\Admin';

    /**
     * @param $email
     * @return AdminEntity|null
     */
    public function getByEmail($email): ?AdminEntity
    {
        return $this->models->admin->getByLogin($email);
    }

    /**
     * @param array $data
     * @param array $social
     * @return object|bool|int|string
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function create(array $data, array $social = []): object|bool|int|string
    {
        $adminId = $this->models->admin->insert([
            'name'          => $data['name'] ?? '',
            'email'         => $data['email'] ?? NULL,
            'mobile'        => $data['mobile'] ?? NULL,
            'mobile_prefix' => $data['mobilePrefix'] ?? NULL,
            'password'      => $data['password'] ?? '',
            'verify'        => $data['verify'] ?? '0',
            'date_of_birth' => $data['date_of_birth'] ?? NULL,
            'created_at'    => DateTimeMethod::datetime(),
            'updated_at'    => DateTimeMethod::datetime(),
        ]);
        Events::trigger('admin_register', (int)$adminId);
        Services::redis()->del("admin:$adminId");
        if($adminId && $social)
            $this->models->oauthConnect->create($adminId, $social['service'], $social['serviceid']);
        return $adminId;
    }

    /**
     * @param AdminEntity  $admin
     * @param              $newPassword
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function setPassword(AdminEntity $admin, $newPassword): bool
    {
        $newPassword = sha1($admin->email . $newPassword);
        return $this->models->admin->update($admin->id, ['password' => $newPassword]);
    }

    /**
     * @param     $params
     * @param int $id
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function updateAdminProfile($params, int $id = 0): bool
    {
        $id     = $id ?: Storage::$admin->id;
        $admin  = $this->models->admin->getById($id);
        $params = is_object($params) ? (array)$params : $params;

        $admin->name     = (string)($params['name'] ?? $admin->name);
        $admin->nickname = (string)($params['nickname'] ?? $admin->nickname);
        $admin->country  = (string)($params['country'] ?? $admin->country);
        $admin->email    = (string)($params['email'] ?? $admin->email);
        $admin->address  = (string)($params['address'] ?? $admin->address);

        if($params['ic_no'] && $admin->icNo != $params['ic_no']) {
            $admin->icNo = $params['ic_no'];
        }

        //date of birth can only update once
        $updateBirthDate = FALSE;
        if(!$admin->dateOfBirth && !empty($params['date_of_birth'])) {
            $admin->dateOfBirth = date('Y-m-d', strtotime($params['date_of_birth']));
            $updateBirthDate    = TRUE;
        }
        $this->models->admin->save($admin);
        Services::redis()->del("admin:$admin->id");

        if($updateBirthDate) {
            Events::trigger('update_birthdate', $id);
        }
        return TRUE;
    }

    /**
     * @param $uploadedFile
     * @return string
     * @throws ClientBadRequest
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function uploadAvatar($uploadedFile): string
    {
        if($uploadedFile instanceof UploadedFile && $uploadedFile->isValid()) {
            $uploadedFile = Provider::resizeImage($uploadedFile->getTempName(), 512, 512, Provider::IMG_RESIZE_SIZE_COVER);
            if(Services::cloudFiles()::upload(Storage::$admin->id . '.png', $uploadedFile, CLOUD_FILES_PATH['adminAvatar'])) {
                Storage::$admin->avatarVer += 1;
                $this->models->admin->save(Storage::$admin);
                return $this->getAvatarUrl(Storage::$admin);
            }
        }
        throw new ClientBadRequest('Upload Avatar failed', "Exceptions.Upload Avatar failed.");
    }

    /**
     * @param AdminEntity $admin
     * @return string
     */
    public function getAvatarUrl(AdminEntity $admin): string
    {
        return $admin->avatarVer > 0 ?
            ('https://' . CLOUD_FILES_CONFIG['cloudFront']['domain'] . '/' . CLOUD_FILES_PATH['adminAvatar'] . '/' . $admin->id . '.png?v=' . $admin->avatarVer) :
            '';
    }

    /**
     * @param string|int $adminId
     * @param string     $action
     * @return int|bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function logAction(string|int $adminId, string $action = ''): bool|int
    {
        $params = Services::request()->getJSON() ?? $_POST;
        $uri    = Services::request()->getUri()->getPath();
        return $this->models->adminLog->insert([
            'admin_id' => $adminId,
            'action'   => $action,
            'param'    => json_encode($params),
            'uri'      => $uri,
        ]);
    }

    /**
     * @param string $view
     * @param array  $params
     * @param string $subject
     * @param string $toEmail
     * @param string $lang
     * @return array|bool|string
     */
    public function sendAdminEmail(string $view, array $params, string $subject, string $toEmail, string $lang = 'en'): bool|array|string
    {
        $params['cdnPath']   = Utilities::getFullUrl('assets/email/');
        $params['copyright'] = Services::modules()->config->backendConfig->get('adminship_copyright', '2023 © Copyright by Admin Panel. All Rights Reserved');

        $params['content'] = view("email/{$view}_$lang", $params);
        $html              = view("email/$view", $params);

        $from = [
            'Name'  => config('Email')->{'fromName'},
            'Email' => config('Email')->{'fromEmail'},
        ];
        $to   = [['Name' => '', 'Email' => $toEmail]];
        return Provider::sendEmail($subject, $html, $from, $to);
    }

    /**
     * @param $adminId
     * @return AdminEntity|null
     */
    public function get($adminId): ?AdminEntity
    {
        return $this->models->admin->getById($adminId);
    }
}