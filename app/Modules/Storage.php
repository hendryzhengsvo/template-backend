<?php
declare(strict_types=1);

namespace App\Modules;

use App\Entities\AdminEntity;
use App\Entities\AgencyEntity;
use App\Entities\VisitorEntity;
use App\Entities\AgencyApiKeyEntity;
use App\Entities\OpenapiClientEntity;

class Storage
{
    /**
     * @var string|null visitor platform
     */
    public static string|null $platform = NULL;

    /**
     * @var VisitorEntity|null Session of current visitor
     */
    public static ?VisitorEntity $visitor = NULL;

    /**
     * @var AdminEntity|null Session of admin user
     */
    public static ?AdminEntity $admin = NULL;

    /**
     * @var array
     */
    public static array $adminActions = [];

    /**
     * @var AgencyEntity|null
     */
    public static ?AgencyEntity $agency = NULL;

    /**
     * @var AgencyApiKeyEntity|null
     */
    public static ?AgencyApiKeyEntity $agencyClient = NULL;

    /**
     * @var OpenapiClientEntity|null
     */
    public static ?OpenapiClientEntity $openClient = NULL;

}