<?php
declare(strict_types=1);

namespace App\Modules;

use Wellous\Ci4Component\Modules\WsModel;

//private-section-begin

use App\Models\Admin;
use App\Models\Todos;
use App\Models\Agency;
use App\Models\Config;
use App\Models\Visitor;
use App\Models\AdminLog;
use App\Models\AdminMenu;
use App\Models\AdminLogin;
use App\Models\AdminRoles;
use App\Models\AdminActions;
use App\Models\AdminWebPush;
use App\Models\AgencyApiKey;
use App\Models\OauthConnect;
use App\Models\AdminRoleMenu;
use App\Models\OpenapiClient;
use App\Models\FrontendConfig;
use App\Models\AdminBiometrics;
use App\Models\AdminRoleActions;
use App\Models\AdminNotification;

/**
 * Class Model
 * @property Admin             $admin
 * @property AdminActions      $adminActions
 * @property AdminBiometrics   $adminBiometrics
 * @property AdminLog          $adminLog
 * @property AdminLogin        $adminLogin
 * @property AdminMenu         $adminMenu
 * @property AdminNotification $adminNotification
 * @property AdminRoleActions  $adminRoleActions
 * @property AdminRoleMenu     $adminRoleMenu
 * @property AdminRoles        $adminRoles
 * @property AdminWebPush      $adminWebPush
 * @property Agency            $agency
 * @property AgencyApiKey      $agencyApiKey
 * @property Config            $config
 * @property FrontendConfig    $frontendConfig
 * @property OauthConnect      $oauthConnect
 * @property OpenapiClient     $openapiClient
 * @property Todos             $todos
 * @property Visitor           $visitor
 */

//private-section-end
class Models extends WsModel
{

}
