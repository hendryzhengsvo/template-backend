<?php
declare(strict_types=1);

namespace App\Modules;

use Config\Services;

/**
 * Pagination Modules
 */
class Pagination
{
    private bool  $enable = FALSE;
    private int   $currentPage;
    private int   $limit;
    private int   $offset;
    private array $data   = [
        'page'        => 1,
        'pageSize'    => 10,
        'limit'       => 10,
        'offset'      => 0,
        'totalRecord' => 0,
        'totalPage'   => 1,
    ];

    public function __construct()
    {
        $this->limit       = (int)Services::request()->getVar('page_size') ?? 10;
        $this->limit       = $this->limit <= 0 ? 10 : $this->limit;
        $this->currentPage = (int)Services::request()->getVar('page') ?? 1;
        $this->currentPage = $this->currentPage <= 0 ? 1 : $this->currentPage;
        $this->offset      = ($this->currentPage > 1) ? (($this->currentPage - 1) * $this->limit) : 0;
    }

    /**
     * @param int $totalRecord
     * @return void
     */
    public function init(int $totalRecord): void
    {
        $this->enable              = TRUE;
        $this->data['page']        = $this->currentPage;
        $this->data['pageSize']    = $this->limit;
        $this->data['limit']       = $this->limit;
        $this->data['offset']      = $this->offset;
        $this->data['totalRecord'] = $totalRecord;
        $this->data['totalPage']   = ceil($totalRecord / $this->limit);
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return array|null
     */
    public function data(): ?array
    {
        return $this->enable ? $this->data : NULL;
    }
}