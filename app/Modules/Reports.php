<?php
declare(strict_types=1);

namespace App\Modules;

class Reports
{
    /**
     * @var string
     */
    protected string $componentPath = '';
    /**
     * @var array
     */
    protected array $components = [];

    final public function __construct()
    {
        $this->componentPath = 'App\ReportModels';
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    final public function __get(string $name)
    {
        $className = ucfirst($name);
        if(isset($this->components[$className])) {
            return $this->components[$className];
        } else {
            if(empty($this->componentPath))
                return NULL;
            else
                $classPath = "$this->componentPath\\$className";
            if(class_exists($classPath))
                $this->components[$className] = new $classPath('slave', FALSE, MYSQLI_USE_RESULT);
            return $this->components[$className] ?? NULL;
        }
    }
}
