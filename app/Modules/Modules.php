<?php
declare(strict_types=1);

namespace App\Modules;

use App\Modules\Logic\Auth;
use App\Modules\Logic\Todo;
use App\Traits\ModuleTrait;
use App\Modules\Logic\Admin;
use App\Modules\Logic\Config;
use App\Modules\Logic\Visitor;
use App\Modules\Logic\Notification;
use Wellous\Ci4Component\Modules\WsModules;

/**
 * Modules Modules
 * @property Auth         $auth
 * @property Admin        $admin
 * @property Config       $config
 * @property Models       $models
 * @property Notification $notification
 * @property Pagination   $pagination
 * @property Todo         $todo
 * @property Visitor      $visitor
 */
class Modules extends WsModules
{
    use ModuleTrait;

    /**
     * Module Construct
     */
    final public function __construct()
    {
        parent::__construct();
        $this->baseModule    = [
            'models'     => 'App\Modules\Models',
            'pagination' => 'App\Modules\Pagination',
        ];
        $this->componentPath = 'App\Modules\Logic';
    }
}