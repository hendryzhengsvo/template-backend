<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AgencyApiKeyEntity
 * @property integer $id
 * @property integer $agencyId
 * @property string  $apiKey
 * @property string  $apiSecret
 * @property integer $status
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class AgencyApiKeyEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'agency_id'  => NULL,
        'api_key'    => NULL,
        'api_secret' => NULL,
        'status'     => 1,
        'created_at' => NULL,
        'updated_at' => NULL,
        'deleted_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'agency_id'  => '?integer',
        'api_key'    => '?string',
        'api_secret' => '?string',
        'status'     => 'integer',
        'created_at' => '?string',
        'updated_at' => '?string',
        'deleted_at' => '?string',
    ];

    protected $datamap = [
        'agencyId'  => 'agency_id',
        'apiKey'    => 'api_key',
        'apiSecret' => 'api_secret',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}