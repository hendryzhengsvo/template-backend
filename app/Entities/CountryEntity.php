<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class CountryEntity
 * @property integer $id
 * @property string  $code
 * @property string  $name
 * @property string  $mobileCode
 * @property string  $mobileFormat
 * @property string  $mobileRegex
 * @property string  $icRegex
 */
class CountryEntity extends BaseEntity
{
    protected $attributes = [
        'id'            => NULL,
        'code'          => NULL,
        'name'          => NULL,
        'mobile_code'   => NULL,
        'mobile_format' => NULL,
        'mobile_regex'  => NULL,
        'ic_regex'      => NULL,
    ];

    protected $casts = [
        'id'            => '?integer',
        'code'          => '?string',
        'name'          => '?string',
        'mobile_code'   => '?string',
        'mobile_format' => '?string',
        'mobile_regex'  => '?string',
        'ic_regex'      => '?string',
    ];

    protected $datamap = [
        'mobileCode'   => 'mobile_code',
        'mobileFormat' => 'mobile_format',
        'mobileRegex'  => 'mobile_regex',
        'icRegex'      => 'ic_regex',
    ];

    protected $dates = [];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}