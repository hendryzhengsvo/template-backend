<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AgencyEntity
 * @property integer $id
 * @property integer $topAgencyId
 * @property string  $name
 * @property string  $gender
 * @property string  $email
 * @property string  $mobile
 * @property string  $password
 * @property string  $dateOfBirth
 * @property integer $roleId
 * @property integer $verify
 * @property integer $expired
 * @property integer $avatarVer
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class AgencyEntity extends BaseEntity
{
    protected $attributes = [
        'id'            => NULL,
        'top_agency_id' => 0,
        'name'          => NULL,
        'gender'        => 'N/A',
        'email'         => NULL,
        'mobile'        => NULL,
        'password'      => NULL,
        'date_of_birth' => '1970-01-01',
        'role_id'       => 0,
        'verify'        => NULL,
        'expired'       => 0,
        'avatarVer'     => 0,
        'created_at'    => '1970-01-01 07:30:00',
        'updated_at'    => '1970-01-01 07:30:00',
        'deleted_at'    => NULL,
    ];

    protected $casts = [
        'id'            => '?integer',
        'top_agency_id' => 'integer',
        'name'          => '?string',
        'gender'        => 'string',
        'email'         => '?string',
        'mobile'        => '?string',
        'password'      => '?string',
        'date_of_birth' => 'string',
        'role_id'       => 'integer',
        'verify'        => '?integer',
        'expired'       => 'integer',
        'avatarVer'     => 'integer',
        'created_at'    => 'string',
        'updated_at'    => 'string',
        'deleted_at'    => '?string',
    ];

    protected $datamap = [
        'topAgencyId' => 'top_agency_id',
        'dateOfBirth' => 'date_of_birth',
        'roleId'      => 'role_id',
        'createdAt'   => 'created_at',
        'updatedAt'   => 'updated_at',
        'deletedAt'   => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}