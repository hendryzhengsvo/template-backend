<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminLogEntity
 * @property integer $id
 * @property integer $adminId
 * @property integer $refId
 * @property string  $refTable
 * @property integer $visitorId
 * @property string  $action
 * @property array   $param
 * @property string  $createdAt
 * @property string  $updatedAt
 */
class AdminLogEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'admin_id'   => 0,
        'ref_id'     => 0,
        'ref_table'  => '',
        'visitor_id' => 0,
        'action'     => '',
        'param'      => NULL,
        'created_at' => NULL,
        'updated_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'admin_id'   => 'integer',
        'ref_id'     => 'integer',
        'ref_table'  => 'string',
        'visitor_id' => 'integer',
        'action'     => 'string',
        'param'      => '?json-array',
        'created_at' => '?string',
        'updated_at' => '?string',
    ];

    protected $datamap = [
        'adminId'   => 'admin_id',
        'refId'     => 'ref_id',
        'refTable'  => 'ref_table',
        'visitorId' => 'visitor_id',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}