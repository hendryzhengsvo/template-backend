<?php
declare(strict_types=1);

namespace App\Entities;

use App\Traits\EntityStrucValue;

//private-section-begin

/**
 * Class FrontendConfigEntity
 * @property integer $id
 * @property string  $platform
 * @property string  $type
 * @property string  $key
 * @property string  $value
 * @property integer $status
 */
class FrontendConfigEntity extends BaseEntity
{
    protected $attributes = [
        'id'       => NULL,
        'platform' => 'admin',
        'type'     => '',
        'key'      => '',
        'value'    => '',
        'status'   => 1,
    ];

    protected $casts = [
        'id'       => '?integer',
        'platform' => 'string',
        'type'     => 'string',
        'key'      => 'string',
        'value'    => 'string',
        'status'   => 'integer',
    ];

    protected $datamap = [

    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }

    use EntityStrucValue;
}