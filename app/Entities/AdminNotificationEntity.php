<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminNotificationEntity
 * @property integer $id
 * @property integer $adminId
 * @property string  $category
 * @property string  $title
 * @property string  $content
 * @property integer $status
 * @property array   $params
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class AdminNotificationEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'admin_id'   => 0,
        'category'   => 'misc',
        'title'      => '',
        'content'    => '',
        'status'     => 0,
        'params'     => '[]',
        'created_at' => '1970-01-01 07:30:00',
        'updated_at' => '1970-01-01 07:30:00',
        'deleted_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'admin_id'   => 'integer',
        'category'   => 'string',
        'title'      => 'string',
        'content'    => 'string',
        'status'     => 'integer',
        'params'     => 'json-array',
        'created_at' => 'string',
        'updated_at' => 'string',
        'deleted_at' => '?string',
    ];

    protected $datamap = [
        'adminId'   => 'admin_id',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }

    /**
     * @return string[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

}