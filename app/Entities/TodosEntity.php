<?php
declare(strict_types=1);

/**
 * Class TodosEntity
 * This class represents a TodosEntity.
 * @package App\Entities
 */

namespace App\Entities;

//private-section-begin

/**
 * Class TodosEntity
 * @property integer $id
 * @property string  $type
 * @property integer $typeId
 * @property string  $title
 * @property string  $description
 * @property integer $isCompleted
 * @property string  $completionTime
 * @property string  $reminderTime
 * @property string  $createdAt
 * @property string  $updatedAt
 */
class TodosEntity extends BaseEntity
{
    protected $attributes = [
        'id'              => NULL,
        'type'            => 'admin',
        'type_id'         => 0,
        'title'           => '',
        'description'     => NULL,
        'is_completed'    => 0,
        'completion_time' => NULL,
        'reminder_time'   => NULL,
        'created_at'      => '1970-01-01 07:30:00',
        'updated_at'      => '1970-01-01 07:30:00',
    ];

    protected $casts = [
        'id'              => '?integer',
        'type'            => 'string',
        'type_id'         => 'integer',
        'title'           => 'string',
        'description'     => '?string',
        'is_completed'    => 'integer',
        'completion_time' => '?string',
        'reminder_time'   => '?string',
        'created_at'      => 'string',
        'updated_at'      => 'string',
    ];

    protected $datamap = [
        'typeId'         => 'type_id',
        'isCompleted'    => 'is_completed',
        'completionTime' => 'completion_time',
        'reminderTime'   => 'reminder_time',
        'createdAt'      => 'created_at',
        'updatedAt'      => 'updated_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}