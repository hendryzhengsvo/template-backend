<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminRoleMenuEntity
 * @property integer $id
 * @property integer $roleId
 * @property integer $menuId
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class AdminRoleMenuEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'role_id'    => 0,
        'menu_id'    => 0,
        'created_at' => NULL,
        'updated_at' => NULL,
        'deleted_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'role_id'    => 'integer',
        'menu_id'    => 'integer',
        'created_at' => '?string',
        'updated_at' => '?string',
        'deleted_at' => '?string',
    ];

    protected $datamap = [
        'roleId'    => 'role_id',
        'menuId'    => 'menu_id',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}