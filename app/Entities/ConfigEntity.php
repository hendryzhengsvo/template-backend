<?php
declare(strict_types=1);

namespace App\Entities;

use App\Traits\EntityStrucValue;

//private-section-begin

/**
 * Class ConfigEntity
 * @property integer $id
 * @property string  $type
 * @property string  $platform
 * @property string  $key
 * @property string  $value
 */
class ConfigEntity extends BaseEntity
{
    protected $attributes = [
        'id'       => NULL,
        'type'     => '',
        'platform' => 'default',
        'key'      => '',
        'value'    => '',
    ];

    protected $casts = [
        'id'       => '?integer',
        'type'     => 'string',
        'platform' => 'string',
        'key'      => 'string',
        'value'    => 'string',
    ];

    protected $datamap = [

    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }

    use EntityStrucValue;
}