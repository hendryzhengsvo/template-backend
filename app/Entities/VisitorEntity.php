<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class VisitorEntity
 * @property integer $id
 * @property string  $code
 * @property string  $platform
 * @property integer $loginId
 * @property integer $status
 * @property integer $loggedAt
 * @property integer $lastseenAt
 * @property integer $firstseenAt
 */
class VisitorEntity extends BaseEntity
{
    protected $attributes = [
        'id'           => NULL,
        'code'         => '',
        'platform'     => 'member',
        'login_id'     => 0,
        'status'       => 1,
        'logged_at'    => 0,
        'lastseen_at'  => 0,
        'firstseen_at' => 0,
    ];

    protected $casts = [
        'id'           => '?integer',
        'code'         => 'string',
        'platform'     => 'string',
        'login_id'     => 'integer',
        'status'       => 'integer',
        'logged_at'    => 'integer',
        'lastseen_at'  => 'integer',
        'firstseen_at' => 'integer',
    ];

    protected $datamap = [
        'loginId'     => 'login_id',
        'loggedAt'    => 'logged_at',
        'lastseenAt'  => 'lastseen_at',
        'firstseenAt' => 'firstseen_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}