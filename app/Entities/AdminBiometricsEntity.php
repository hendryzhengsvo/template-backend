<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminBiometricsEntity
 * @property integer $id
 * @property integer $adminId
 * @property string  $credentialId
 * @property array   $credential
 * @property string  $deletedAt
 * @property string  $createdAt
 */
class AdminBiometricsEntity extends BaseEntity
{
    protected $attributes = [
        'id'            => NULL,
        'admin_id'      => 0,
        'credential_id' => '',
        'credential'    => '[]',
        'deleted_at'    => NULL,
        'created_at'    => '1970-01-01 07:30:00',
    ];

    protected $casts = [
        'id'            => '?integer',
        'admin_id'      => 'integer',
        'credential_id' => 'string',
        'credential'    => 'json-array',
        'deleted_at'    => '?string',
        'created_at'    => 'string',
    ];

    protected $datamap = [
        'adminId'      => 'admin_id',
        'credentialId' => 'credential_id',
        'deletedAt'    => 'deleted_at',
        'createdAt'    => 'created_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}