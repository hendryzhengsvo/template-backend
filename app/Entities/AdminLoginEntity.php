<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminLoginEntity
 * @property integer $id
 * @property integer $adminId
 * @property string  $source
 * @property string  $useragent
 * @property string  $browser
 * @property string  $platform
 * @property string  $loginIp
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class AdminLoginEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'admin_id'   => 0,
        'source'     => 'login',
        'useragent'  => '',
        'browser'    => '',
        'platform'   => '',
        'login_ip'   => '',
        'created_at' => NULL,
        'updated_at' => NULL,
        'deleted_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'admin_id'   => 'integer',
        'source'     => 'string',
        'useragent'  => 'string',
        'browser'    => 'string',
        'platform'   => 'string',
        'login_ip'   => 'string',
        'created_at' => '?string',
        'updated_at' => '?string',
        'deleted_at' => '?string',
    ];

    protected $datamap = [
        'adminId'   => 'admin_id',
        'loginIp'   => 'login_ip',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}