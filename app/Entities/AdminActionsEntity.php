<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminActionsEntity
 * @property integer $id
 * @property string  $name
 * @property string  $code
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class AdminActionsEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'name'       => '',
        'code'       => '',
        'created_at' => NULL,
        'updated_at' => NULL,
        'deleted_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'name'       => 'string',
        'code'       => 'string',
        'created_at' => '?string',
        'updated_at' => '?string',
        'deleted_at' => '?string',
    ];

    protected $datamap = [
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}