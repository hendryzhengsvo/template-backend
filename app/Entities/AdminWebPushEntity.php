<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminWebPushEntity
 * @property integer $id
 * @property integer $adminId
 * @property array   $subscription
 * @property string  $endpoint
 * @property array   $enckey
 * @property integer $status
 * @property string  $updatedAt
 * @property string  $createdAt
 */
class AdminWebPushEntity extends BaseEntity
{
    protected $attributes = [
        'id'           => NULL,
        'admin_id'     => 0,
        'subscription' => '[]',
        'endpoint'     => '',
        'enckey'       => '[]',
        'status'       => 1,
        'updated_at'   => '1970-01-01 07:30:00',
        'created_at'   => '1970-01-01 07:30:00',
    ];

    protected $casts = [
        'id'           => '?integer',
        'admin_id'     => 'integer',
        'subscription' => 'json-array',
        'endpoint'     => 'string',
        'enckey'       => 'json-array',
        'status'       => 'integer',
        'updated_at'   => 'string',
        'created_at'   => 'string',
    ];

    protected $datamap = [
        'adminId'   => 'admin_id',
        'updatedAt' => 'updated_at',
        'createdAt' => 'created_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}