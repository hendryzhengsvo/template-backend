<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminMenuEntity
 * @property integer $id
 * @property integer $parentId
 * @property integer $roleId
 * @property string  $icon
 * @property string  $name
 * @property string  $path
 * @property integer $sort
 * @property integer $status
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class AdminMenuEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'parent_id'  => 0,
        'role_id'    => 0,
        'icon'       => NULL,
        'name'       => NULL,
        'path'       => NULL,
        'sort'       => 1,
        'status'     => 1,
        'created_at' => NULL,
        'updated_at' => NULL,
        'deleted_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'parent_id'  => 'integer',
        'role_id'    => 'integer',
        'icon'       => '?string',
        'name'       => '?string',
        'path'       => '?string',
        'sort'       => 'integer',
        'status'     => 'integer',
        'created_at' => '?string',
        'updated_at' => '?string',
        'deleted_at' => '?string',
    ];

    protected $datamap = [
        'parentId'  => 'parent_id',
        'roleId'    => 'role_id',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }

    /**
     * @return array
     */
    function outputItem(): array
    {
        return [
            'icon'     => $this->icon,
            'name'     => $this->name,
            'path'     => $this->path,
            'children' => [],
        ];
    }
}