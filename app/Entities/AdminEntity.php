<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class AdminEntity
 * @property integer $id
 * @property string  $name
 * @property string  $nickname
 * @property string  $email
 * @property string  $password
 * @property array   $homeRoute
 * @property integer $status
 * @property integer $roleId
 * @property integer $avatarVer
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class AdminEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'name'       => '',
        'nickname'   => NULL,
        'email'      => '',
        'password'   => '',
        'home_route' => NULL,
        'status'     => 1,
        'role_id'    => 1,
        'avatar_ver' => 0,
        'created_at' => '1970-01-01 07:30:00',
        'updated_at' => '1970-01-01 07:30:00',
        'deleted_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'name'       => 'string',
        'nickname'   => '?string',
        'email'      => 'string',
        'password'   => 'string',
        'home_route' => '?json-array',
        'status'     => 'integer',
        'role_id'    => 'integer',
        'avatar_ver' => 'integer',
        'created_at' => 'string',
        'updated_at' => 'string',
        'deleted_at' => '?string',
    ];

    protected $datamap = [
        'homeRoute' => 'home_route',
        'roleId'    => 'role_id',
        'avatarVer' => 'avatar_ver',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }

    public function toFrontend(): array
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'nickname'   => '',
            'email'      => $this->email,
            'homeRoute'  => $this->homeRoute,
            'avatar'     => !empty(CLOUD_FILES_CONFIG['cloudFront']['domain']) && !empty(CLOUD_FILES_PATH['adminAvatar'])
                ? 'https://' . CLOUD_FILES_CONFIG['cloudFront']['domain'] . '/' . CLOUD_FILES_PATH['adminAvatar'] . '/' . $this->id . '.png?v=' . $this->avatarVer
                : '',
            'avatar_ver' => $this->avatarVer,
            'created_at' => $this->createdAt,
            'updated_at' => $this->updatedAt,
        ];
    }
}