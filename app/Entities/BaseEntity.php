<?php
declare(strict_types=1);

namespace App\Entities;

use Wellous\Ci4Component\Entities\WsBaseEntity;

class BaseEntity extends WsBaseEntity
{
}