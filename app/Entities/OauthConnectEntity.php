<?php
declare(strict_types=1);

namespace App\Entities;

//private-section-begin

/**
 * Class OauthConnectEntity
 * @property integer $id
 * @property integer $roleid
 * @property string  $panel
 * @property string  $service
 * @property string  $serviceid
 * @property string  $createdAt
 * @property string  $updatedAt
 * @property string  $deletedAt
 */
class OauthConnectEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'roleid'     => 0,
        'panel'      => 'admin',
        'service'    => '',
        'serviceid'  => '',
        'created_at' => NULL,
        'updated_at' => NULL,
        'deleted_at' => NULL,
    ];

    protected $casts = [
        'id'         => '?integer',
        'roleid'     => 'integer',
        'panel'      => 'string',
        'service'    => 'string',
        'serviceid'  => 'string',
        'created_at' => '?string',
        'updated_at' => '?string',
        'deleted_at' => '?string',
    ];

    protected $datamap = [
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at',
    ];

//private-section-end

    /**
     * Entity constructor.
     * @param array|null $data
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}