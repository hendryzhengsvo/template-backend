<?php
declare(strict_types=1);

/**
 * Class OpenapiClientEntity
 * This class represents a OpenapiClientEntity.
 * @package App\Entities
 */

namespace App\Entities;

//private-section-begin

/**
 * Class OpenapiClientEntity
 * @property integer $id
 * @property string  $name
 * @property string  $apiKey
 * @property string  $apiSecret
 * @property integer $status
 * @property string  $createdAt
 */
class OpenapiClientEntity extends BaseEntity
{
    protected $attributes = [
        'id'         => NULL,
        'name'       => '',
        'api_key'    => '',
        'api_secret' => '',
        'status'     => 1,
        'created_at' => '1970-01-01 07:30:00',
    ];

    protected $casts = [
        'id'         => '?integer',
        'name'       => 'string',
        'api_key'    => 'string',
        'api_secret' => 'string',
        'status'     => 'integer',
        'created_at' => 'string',
    ];

    protected $datamap = [
        'apiKey'    => 'api_key',
        'apiSecret' => 'api_secret',
        'createdAt' => 'created_at',
    ];

//private-section-end

    /**
     * Constructs of entity
     * @param array|null $data The data to initialize the entity with. Defaults to null if not provided.
     * @return void
     */
    public function __construct(?array $data = NULL)
    {
        parent::__construct($data);
    }
}