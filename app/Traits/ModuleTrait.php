<?php
declare(strict_types=1);

namespace App\Traits;

use Wellous\Ci4Component\Traits\WsTraitModule;

trait ModuleTrait
{
    use WsTraitModule;
}