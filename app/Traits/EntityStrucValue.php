<?php
declare(strict_types=1);

namespace App\Traits;

use Wellous\Ci4Component\Traits\WsTraitStrucValue;

/**
 * Trait WsEntityFlagSet
 * @package Wellous\Ci4Component\Traits
 * @property  array $attributes
 */
trait EntityStrucValue
{
    use WsTraitStrucValue;
}