<?php
declare(strict_types=1);

namespace App\Traits;

use Wellous\Ci4Component\Traits\WsTraitFlagSet;

trait EntityFlagSet
{
    use WsTraitFlagSet;
}