<?php
declare(strict_types=1);

namespace App\Traits;

trait ConfigTrait
{
    /**
     * @param string $value
     * @param string $type
     * @return mixed
     */
    protected function returnByType(string $value, string $type): mixed
    {
        $type = empty($type) ? 'string' : $type;
        return match ($type) {
            'json'            => json_decode($value),
            'json-array'      => json_decode($value, TRUE),
            'number-array'    => array_map('intval', explode(',', $value)),
            'list-array'      => explode(',', $value),
            'integer'         => (int)$value,
            'boolean'         => boolval($value),
            'date'            => date("Y-m-d", strtotime($value)),
            'time'            => date("H:i:s", strtotime($value)),
            'datetime'        => date("Y-m-d H:i:s", strtotime($value)),
            'timestamp'       => $this->isValidTimeStamp($value) ? $value : strtotime($value),
            'double', 'float' => (double)$value,
            default           => $value,
        };
    }

    /**
     * @param string $timestamp
     * @return bool
     */
    protected function isValidTimeStamp(string $timestamp): bool
    {
        return ((string)(int)$timestamp === $timestamp)
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }

    /**
     * @param mixed $configValue
     * @return string
     */
    protected function getType(mixed $configValue): string
    {
        $type = gettype($configValue);
        if(in_array($type, ['resource', 'resource (closed)', 'object', 'NULL', 'unknown type'], TRUE)) {
            return 'json';
        } elseif($type === 'array') {
            if(!array_is_list($configValue)) {
                return 'json-array';
            } else {
                if(count($configValue) === count(array_filter($configValue, function($val)
                    {
                        return preg_match('/^-?[1-9][0-9]*$/', (string)$val) === 1;
                    }))) {
                    return 'number-array';
                } else {
                    return 'list-array';
                }
            }
        } else {
            if(is_bool($configValue)) {
                return 'boolean';
            } elseif(preg_match('/^-?[1-9][0-9]*$/', (string)$configValue) === 1 || $configValue === 0) {
                return 'integer';
            } elseif(is_numeric($configValue)) {
                return 'double';
            } else {
                return $type;
            }
        }
    }

    /**
     * @param mixed  $value
     * @param string $type
     * @return bool|int|string
     */
    protected function saveTypeValue(mixed $value, string $type): bool|int|string
    {
        return match ($type) {
            'json', 'json-array' => json_encode($value, JSON_UNESCAPED_SLASHES & JSON_UNESCAPED_UNICODE),
            'number-array'       => implode(',', array_map('intval', $value)),
            'list-array'         => implode(',', $value),
            'boolean'            => $value ? '1' : '0',
            'date'               => date("Y-m-d", strtotime($value)),
            'time'               => date("H:i:s", strtotime($value)),
            'datetime'           => date("Y-m-d H:i:s", strtotime($value)),
            'timestamp'          => (int)($this->isValidTimeStamp($value) ? $value : strtotime($value)),
            default              => (string)$value,
        };
    }
}