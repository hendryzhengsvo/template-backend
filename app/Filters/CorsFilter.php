<?php
declare(strict_types=1);

namespace App\Filters;

use Config\Services;
use App\Modules\Storage;
use App\Libraries\Utilities;
use App\Exceptions\ComingSoon;
use App\Exceptions\Maintenance;
use App\Libraries\DateTimeMethod;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Filters\FilterInterface;
use Wellous\Ci4Component\Filters\WsFilterCors;

class CorsFilter extends WsFilterCors implements FilterInterface
{
    private array $silent = [
        'member/initial',
    ];

    /**
     * @param RequestInterface $request
     * @param null             $arguments
     * @return void
     * @throws Maintenance
     * @throws ComingSoon
     */
    public function before(RequestInterface $request, $arguments = NULL): void
    {
        $response = Services::response();
        if($request->hasHeader('Origin'))
            $response->setHeader('Access-Control-Expose-Headers', 'Visitorid, DeviceId, Lang');
        parent::before($request, $arguments);

        Storage::$platform = $request->getHeaderLine('platform');
        $this->checkMaintenance();
        $this->checkComingSoon();
    }

    /**
     * @return void
     * @throws Maintenance
     */
    private function checkMaintenance(): void
    {
        //mode - immediate, schedule, disable
        $maintenance = Services::modules()->config->backendConfig->get('maintenance', []);
        if(is_array($maintenance) && !empty($maintenance['mode'])) {
            $goMaintenance = FALSE;
            if((string)$maintenance['mode'] === 'immediate')
                $goMaintenance = TRUE;
            if((string)$maintenance['mode'] === 'schedule') {
                if(!empty($maintenance['schedule'])) {
                    foreach($maintenance['schedule'] as $schedule) {
                        if($schedule['start'] <= DateTimeMethod::datetime() &&
                            $schedule['end'] >= DateTimeMethod::datetime()) {
                            $goMaintenance = TRUE;
                            break;
                        }
                    }
                }
            }
            if($goMaintenance) {
                $response  = [];
                $ip        = Services::getClientIp();
                $whiteList = Services::modules()->config->backendConfig->get('memberWhiteList', []);
                if(Utilities::isUriMatched(Services::getRoutePath(), $this->silent))
                    $response = ['data' => ['config' => Services::modules()->config->frontendConfig->all(Storage::$platform)]];
                if(!in_array($ip, $whiteList, TRUE))
                    throw new Maintenance(addOnResponse: $response);
            }
        }
    }

    /**
     * @return void
     * @throws ComingSoon
     */
    private function checkComingSoon(): void
    {
        if(Services::modules()->config->backendConfig->get('comingsoon', FALSE)) {
            $response = [];
            if(Utilities::isUriMatched(Services::getRoutePath(), $this->silent))
                $response = ['data' => ['config' => Services::modules()->config->frontendConfig->all(Storage::$platform)]];
            throw new ComingSoon(addOnResponse: $response);
        }
    }

    protected function setLang(RequestInterface $request): void
    {
        if(!empty($request->getHeaderLine('Lang'))) {
            $lang      = $request->getHeaderLine('Lang');
            $supported = config('App')->supportedLocales ?? [];
            if(!in_array($lang, $supported, TRUE))
                $lang = config('App')->supportedLocales[0] ?? config('App')->{'defaultLocale'};
            Services::language()->setLocale($lang);
        }
    }
}
