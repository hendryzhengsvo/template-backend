<?php
declare(strict_types=1);

namespace App\Filters;

use Config\Services;
use App\Modules\Storage;
use ReflectionException;
use App\Libraries\DateTimeMethod;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Exceptions\ClientTooManyRequests;

/**
 * Class VisitorFilter
 * Implements the FilterInterface and provides functionality for visitor filtering.
 */
class VisitorFilter implements FilterInterface
{
    /**
     * @var int $rateLimitPeriod in seconds, default 60 seconds
     */
    private int $rateLimitPeriod = 60;

    /**
     * @var int $rateLimit default 20 requests per $rateLimitPeriod
     */
    private int $rateLimit = 20;

    /**
     * @param RequestInterface $request                            The request object
     * @param mixed            $arguments                          Optional arguments
     * @return RequestInterface|ResponseInterface|RedirectResponse The modified request object or a response object or
     *                                                             a redirect response object
     * @throws ClientTooManyRequests If the request limit has been reached
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function before(RequestInterface $request, $arguments = NULL): RequestInterface|ResponseInterface|RedirectResponse
    {
        if(is_cli() || !$request instanceof IncomingRequest)
            return $request;

        //If not member request, skip
        $visitorId = $request->getHeaderLine('VisitorId');

        if(empty($visitorId))
            return $request;

        if(empty(Storage::$platform))
            Storage::$platform = 'admin';

        if(!preg_match('/^[a-zA-Z0-9]{60,}$/', $visitorId))
            $visitorId = '';

        //Get visitor
        Services::modules()->visitor->visit($visitorId, Storage::$platform);

        //Rate limit
        $this->rateLimitValidate($request);

        return $request;
    }

    /**
     * Validates the rate limit for a given request.
     * @param RequestInterface $request The request to validate.
     * @throws ClientTooManyRequests If the rate limit is exceeded.
     */
    private function rateLimitValidate(RequestInterface $request): void
    {
        $uri   = $request->getUri()->getPath();
        $redis = Services::redis();
        if(Storage::$visitor) {
            $preKey = "rate:" . Storage::$visitor->code;
            if($redis->exists("$preKey:limit"))
                throw new ClientTooManyRequests("Limit lock");

            $now       = DateTimeMethod::timestamp();
            $lockTime  = $now % $this->rateLimitPeriod;
            $timeBlock = $now - $lockTime;

            $key = "$preKey:request:$timeBlock";
            $redis->hincrby($key, $uri, 1);
            $redis->expire($key, $this->rateLimitPeriod);
            if((int)$redis->hget($key, $uri) > $this->rateLimit) {
                $redis->del($key);
                $redis->set("$preKey:limit", time(), 'EX', $lockTime);
                throw new ClientTooManyRequests("Reach request limit");
            }
        }
    }

    /**
     * Sets the 'VisitorId' header in the response if a visitor is present in the storage.
     * @param RequestInterface  $request   The HTTP request object.
     * @param ResponseInterface $response  The HTTP response object.
     * @param mixed             $arguments Additional arguments (optional).
     * @return void
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL): void
    {
        if(Storage::$visitor)
            Services::response()->setHeader('VisitorId', Storage::$visitor->code);
    }
}