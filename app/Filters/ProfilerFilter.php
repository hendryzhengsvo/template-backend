<?php
declare(strict_types=1);

namespace App\Filters;

use Config\Services;
use App\Libraries\Provider;
use App\Libraries\Utilities;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Filters\FilterInterface;
use Wellous\Ci4Component\Filters\WsFilterProfiler;

/**
 * Class ProfilerFilter
 * The ProfilerFilter class extends the WsFilterProfiler class and implements the FilterInterface.
 * It performs necessary initializations before processing the request.
 * @package Namespace\To\ProfilerFilter
 */
class ProfilerFilter extends WsFilterProfiler implements FilterInterface
{
    /**
     * Perform necessary initializations before processing the request.
     * @param RequestInterface $request   The request object being processed.
     * @param mixed|null       $arguments Optional arguments.
     * @return void
     */
    final public function before(RequestInterface $request, $arguments = NULL): void
    {
        parent::before($request, $arguments);
        Services::cloudFiles()::initial(CLOUD_FILES_CONFIG['bucket'], CLOUD_FILES_CONFIG['cloudFront']);
        Services::twoFactor()::initial((config('App')->secret ?? Utilities::genCode()));
        Services::profiler()::initial(!isProduction() || Services::modules()->config->backendConfig->get('open_profiler', 0) >= time());
        Provider::initial(Services::modules()->config->backendConfig->get('provider', []));
    }
}