<?php
declare(strict_types=1);

namespace App\Filters;

use Config\Services;
use App\Modules\Storage;
use App\Libraries\Utilities;
use App\Entities\OpenapiClientEntity;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Wellous\Ci4Component\Exceptions\ClientUnauthorized;

/**
 * Class ApiAuthFilter
 * @package App\Filters
 */
class OpenApiAuthFilter implements FilterInterface
{
    //default = 300
    private int $rangeLimit = 300;

    private array $allowHmacHash = ['sha256', 'sha384', 'sha512', 'sha3-256', 'sha3-512'];

    private array $silent = [
        'openapi/notification/add',
    ];

    /**
     * @param RequestInterface $request
     * @param null             $arguments
     * @return RequestInterface
     * @throws ClientUnauthorized
     */
    public function before(RequestInterface $request, $arguments = NULL): RequestInterface
    {
        $skip = $arguments[0] ?? '' === 'skip';
        $uri  = Services::getRoutePath();
        if($skip || is_cli() || (!str_starts_with(Services::getRoutePath(), 'api/')))
            return $request;

        $models = Services::models();

        $auth = Utilities::parseToken((string)$request->getServer('HTTP_AUTHORIZATION'), $this->allowHmacHash);
        if($auth === FALSE)
            throw new ClientUnauthorized("", "Bad Authorization header");

        if(isProduction() && !in_array(Services::getRoutePath(), $this->silent) &&
            ((int)$auth['ts'] - $this->rangeLimit > time() || (int)$auth['ts'] + $this->rangeLimit < time()))
            throw new ClientUnauthorized("", "Authorization timestamp (ts) must use current timestamp");

        if(!in_array($auth['hash'], $this->allowHmacHash, TRUE))
            throw new ClientUnauthorized("", "Authorization hash method not allowed/found, only accept 'sha256', 'sha384', 'sha256', 'sha512', 'sha3-256', 'sha3-512'");

        $redis = Services::redis();
        if(isProduction() && !in_array($uri, $this->silent)) {
            $nonceCacheName = "nonce:{$auth['apikey']}";
            if(!preg_match("/([a-z\d]{20,40})/i", $auth['nonce']) || !$redis->hsetnx($nonceCacheName, $auth['nonce'], $auth['nonce']))
                throw new ClientUnauthorized("", "Authorization nonce has already used or nonce invalid, nonce must alphanumeric(len: 20~40)");
            $redis->expire($nonceCacheName, $this->rangeLimit);
        }

        /** @var OpenapiClientEntity $openClient */
        $openClient = $models->openapiClient->getByApiKey($auth['apikey']);
        if(empty($openClient))
            throw new ClientUnauthorized("", "Authorization unkown apikey, please check your apikey");

        $signature = hash_hmac($auth['hash'],
            $auth['ts'] . $auth['apikey'] . $auth['nonce'],
            $auth['nonce'] . $openClient->apiSecret
        );
        if($signature !== $auth['signature'])
            throw new ClientUnauthorized("", "Sorry signature is invalid.");

        Storage::$openClient = $openClient;
        return $request;
    }

    /**
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param                   $arguments
     * @return void
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL): void {}
}