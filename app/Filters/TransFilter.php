<?php
declare(strict_types=1);

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use Wellous\Ci4Component\Filters\WsFilterTrans;

/**
 * Represents a translation filter.
 * This class extends the WsFilterTrans class and implements the FilterInterface.
 * @since 1.0.0
 */
class TransFilter extends WsFilterTrans implements FilterInterface
{
}