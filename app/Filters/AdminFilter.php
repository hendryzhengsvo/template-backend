<?php
declare(strict_types=1);

namespace App\Filters;

use Throwable;
use Config\Services;
use App\Modules\Storage;
use App\Entities\AdminEntity;
use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Wellous\Ci4Component\Exceptions\ClientUnauthorized;

/**
 * Class AdminFilter
 * This class is responsible for filtering admin requests.
 * @implements FilterInterface
 */
class AdminFilter implements FilterInterface
{
    /**
     * @var array|string[]
     */
    private array $silent = [
        'admin/initial',
        'admin/notification/pushNotification',
    ];

    /**
     * @param RequestInterface $request
     * @param mixed|null       $arguments
     * @return RequestInterface|ResponseInterface|RedirectResponse
     * @throws ClientUnauthorized
     */
    public function before(RequestInterface $request, $arguments = NULL): RequestInterface|ResponseInterface|RedirectResponse
    {
        $uri = Services::getRoutePath();
        if(is_cli() || !str_starts_with($uri, 'admin/'))
            return $request;

        if(empty(Storage::$visitor->loginId) && !$this->isUriMatched($uri, $this->silent))
            throw new ClientUnauthorized('Device is not login', 'Exceptions.Session expired');

        if(!empty(Storage::$visitor->loginId)) {
            if(!($admin = Services::redis()->getUnserialize('admin:' . Storage::$visitor->loginId)))
                Storage::$admin = Services::modules()->admin->get(Storage::$visitor->loginId);
            else
                Storage::$admin = (new AdminEntity($admin))->syncOriginal();

            if(Storage::$admin->status !== 1 && !in_array($uri, $this->silent)) {
                try {
                    Services::modules()->visitor->logoutVisitor();
                    Services::models()->completeTrans();
                } catch(Throwable) {
                }
                throw new ClientUnauthorized('Status Inactive', 'Exceptions.Status inactive');
            }
        }

        return $request;
    }

    /**
     * Checks if the given URI matches any of the patterns in the except array.
     * @param string $uri    The URI to be checked.
     * @param array  $except An array of URI patterns to be excluded from matching.
     * @return bool Returns TRUE if the URI matches any of the patterns, otherwise FALSE.
     */
    public function isUriMatched(string $uri, array $except): bool
    {
        foreach($except as $pattern) {
            $pattern = str_replace(['/', '*'], ['\/', '(.[^\/]*)'], $pattern);
            if(preg_match('/^' . $pattern . '$/', $uri))
                return TRUE;
        }
        return FALSE;
    }

    /**
     * Logs the user request after it has been processed.
     * @param RequestInterface  $request   The request object containing information about the user's request.
     * @param ResponseInterface $response  The response object containing information about the response to be sent.
     * @param mixed             $arguments (optional) Additional arguments passed to the method. Default is NULL.
     * @return void
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL): void
    {
        //TODO: Log User Request
    }
}

