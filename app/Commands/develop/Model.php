<?php
declare(strict_types=1);

namespace App\Commands\Develop;

use Config\Services;
use App\Commands\Base;
use App\Libraries\Builder;
use Psr\Log\LoggerInterface;
use CodeIgniter\CLI\Commands;

class Model extends Base
{
    protected $group       = 'develop';
    protected $name        = 'dev:model';
    protected $description = 'Create/Update model and entity and Rebuild Models List';
    protected $usage       = 'model [options]';
    protected $arguments   = [];
    protected $options     = [
        'dev:model create [optional]table' => 'create all/selected table non-exists model and entity',
        'dev:model update [optional]table' => 'update all/selected table existing model and entity',
        'dev:model rebuild'                => 'rebuild module model php comment @property',
    ];

    protected Builder $builder;

    /**
     * @param LoggerInterface $logger
     * @param Commands        $commands ]
     */
    public function __construct(LoggerInterface $logger, Commands $commands)
    {
        parent::__construct($logger, $commands);
        $this->builder = new Builder();
        $this->builder->setMapEntityColumn([
            'p_id' => 'productId',
            'd_id' => 'dealerId',
            'o_id' => 'orderId',
            'u_id' => 'userId',
        ]);
        $this->builder->setMapTableColumn([
            'product' => ['p_id' => 'id'],
            'orders'  => ['o_id' => 'id'],
            'dealer'  => ['d_id' => 'id'],
            'user'    => ['u_id' => 'id'],
        ]);
    }

    /**
     * Executes the run method.
     * @param array $params An array of parameters.
     * @return void
     */
    public function run(array $params): void
    {
        if(isset($params[0])) {
            $method = array_shift($params);
            if(method_exists($this, $method))
                $this->$method(...$params);
            else
                Services::cli()->error("Method $method not found");
        } else
            Services::cli()->error('Please specify method');
    }

    public function rebuild(): void
    {
        $this->builder->rebuildModule();
    }

    public function create(?string $table = NULL): void
    {
        if(!empty($table))
            $this->builder->createModel($table)
                ->createEntity($table);
        else
            $this->builder->createAll();
    }

    /**
     * create model and entity
     * @return void
     */
    public function update(): void
    {
        if(!empty($table))
            $this->builder->updateModel($table)
                ->updateEntity($table);
        else
            $this->builder->updateAll();
    }
}
