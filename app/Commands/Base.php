<?php
declare(strict_types=1);

namespace App\Commands;

use Psr\Log\LoggerInterface;
use CodeIgniter\CLI\Commands;
use CodeIgniter\CLI\BaseCommand;

abstract class Base extends BaseCommand
{
    public function __construct(LoggerInterface $logger, Commands $commands)
    {
        set_time_limit(0);
        ini_set('memory_limit', '1G');
        parent::__construct($logger, $commands);
    }
}