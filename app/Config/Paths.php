<?php
declare(strict_types=1);

namespace Config;

require_once WSPATH . 'Config' . DIRECTORY_SEPARATOR . 'WsCfgPaths.php';

use Wellous\Ci4Component\Config\WsCfgPaths;

/**
 * Paths
 * Holds the paths that are used by the system to
 * locate the main directories, app, system, etc.
 * Modifying these allows you to restructure your application,
 * share a system folder between multiple applications, and more.
 * All paths are relative to the project's root folder.
 */
class Paths extends WsCfgPaths
{
    /**
     * Class Constructor
     * This method initializes the object when it is created.
     * @return void
     */
    public function __construct()
    {
        /**
         * --------------------------------------------------------------------
         * Project name
         * --------------------------------------------------------------------
         */
        defined('APP_NAME') || define('APP_NAME', 'TEMPLATE');

        $this->writableDirectory = (string)realpath(sys_get_temp_dir() . DIRECTORY_SEPARATOR . APP_NAME);
    }
}