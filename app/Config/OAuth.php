<?php
declare(strict_types=1);

namespace Config;

use CodeIgniter\Config\BaseConfig;

class OAuth extends BaseConfig
{
    public array $facebook = [
        'client_id'     => '::number::', // Replace with your facebook app id
        'client_secret' => '::string::', // Replace with your facebook app secret
        'endpoint'      => 'https://www.facebook.com/v13.0/dialog/oauth',
        'apiUrl'        => 'https://graph.facebook.com/v13.0',
        'scopes'        => [
            'public_profile', 'email',
        ],
    ];

    public array $google = [
        'client_id'     => '::string::', // Replace with your google app id
        'client_secret' => '::string::', // Replace with your google app secret
        'endpoint'      => 'https://accounts.google.com/o/oauth2/auth',
        'apiUrl'        => 'https://oauth2.googleapis.com',
        'scopes'        => [
            'profile', 'email', 'openid',
        ],
    ];
}