<?php
declare(strict_types=1);

namespace Config;

require_once WSPATH . 'Config' . DIRECTORY_SEPARATOR . 'WsCfgModules.php';

use Wellous\Ci4Component\Config\WsCfgModules;

class Modules extends WsCfgModules
{
}