<?php

namespace Config;

use Wellous\Ci4Component\Config\WsCfgExceptions;

/**
 * Setup how the exception handler works.
 */
class Exceptions extends WsCfgExceptions
{
}