<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgApp;

class App extends WsCfgApp
{
    /**
     * @var string
     */
    public string $secret = "h4d6d3jm7d3fw22b"; //You can change this value

    /**
     * --------------------------------------------------------------------------
     * Default Locale
     * --------------------------------------------------------------------------
     * The Locale roughly represents the language and location that your visitor
     * is viewing the site from. It affects the language strings and other
     * strings (like currency markers, numbers, etc), that your program
     * should run under for this request.
     */
    public string $defaultLocale = 'en-US';

    /**
     * --------------------------------------------------------------------------
     * Supported Locales
     * --------------------------------------------------------------------------
     * If $negotiateLocale is true, this array lists the locales supported
     * by the application in descending order of priority. If no match is
     * found, the first locale will be used.
     * IncomingRequest::setLocale() also uses this list.
     * @var string[]
     */
    public array $supportedLocales = ['en-US', 'zh-CN'];

    /**
     * --------------------------------------------------------------------------
     * Predis configuration
     * --------------------------------------------------------------------------
     * Predis is a flexible and feature-complete PHP client library for the
     * Redis key-value store.
     * @var array $predis
     */
    public array $predis = REDIS_OPTIONS;

    public function __construct()
    {
        $host          = $_SERVER['HTTP_HOST'] ?? 'localhost';
        $this->baseURL = match (ENVIRONMENT) {
            'live'        => "https://api.example.com/",
            'staging'     => "https://api-staging.example.com",
            'development' => "https://api-dev.example.com",
            default       => "http://$host/",
        };
        parent::__construct();
    }
}