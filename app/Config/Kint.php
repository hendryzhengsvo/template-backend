<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgKint;

/**
 * --------------------------------------------------------------------------
 * Kint
 * --------------------------------------------------------------------------
 * We use Kint's `RichRenderer` and `CLIRenderer`. This area contains options
 * that you can set to customize how Kint works for you.
 * @see https://kint-php.github.io/kint/ for details on these settings.
 */
class Kint extends WsCfgKint
{
}