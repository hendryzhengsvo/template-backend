<?php

namespace Config;

use Wellous\Ci4Component\Config\WsCfgContentSecurityPolicy;

/**
 * Stores the default settings for the ContentSecurityPolicy, if you
 * choose to use it. The values here will be read in and set as defaults
 * for the site. If needed, they can be overridden on a page-by-page basis.
 * Suggested reference for explanations:
 * @see https://www.html5rocks.com/en/tutorials/security/content-security-policy/
 */
class ContentSecurityPolicy extends WsCfgContentSecurityPolicy
{
}