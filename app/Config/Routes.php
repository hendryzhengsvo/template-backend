<?php
declare(strict_types=1);

namespace Config;

use SplFileInfo;
use App\Libraries\Profiler;
use CodeIgniter\Files\FileCollection;
use Wellous\Ci4Component\Tool\WsRouteCollection as RouteCollection;

/**
 * @var RouteCollection $routes
 */

/**
 * Class Routes
 * @package Config\Route
 */
class Routes
{
    /**
     * @param RouteCollection $routes
     * @return void
     */
    public static function routes(RouteCollection $routes): void
    {
        Profiler::addCheckPoint('routes');

        // Load the system's routing file first, so that the app and ENVIRONMENT
        // can override as needed.
        if(file_exists(SYSTEMPATH . 'Config/Routes.php'))
            require SYSTEMPATH . 'Config/Routes.php';

        /*
         * --------------------------------------------------------------------
         * Additional Routing
         * --------------------------------------------------------------------
         *
         * There will often be times that you need additional routing and you
         * need it to be able to override any defaults in this file. Environment
         * based routes is one such time. require() additional route files here
         * to make that happen.
         *
         * You will have access to the $routes object within that file without
         * needing to reload it.
         */

        $routeFiles = new FileCollection([__DIR__ . DIRECTORY_SEPARATOR . 'Route']);
        /** @var SplFileInfo $routeFile */
        foreach($routeFiles as $routeFile) {
            $className = $routeFile->getBasename('.php');
            if(!in_array($className, ['.', '..'])) {
                $routeClass = "App\\Config\\Route\\$className";
                if(class_exists($routeClass))
                    $routeClass::routes($routes);
            }
        }

        $request = Services::request();
        Profiler::addLog('routes', "Routes loaded with uri: " . $request->getMethod() . ' ' . Services::getRoutePath());
    }
}

/**
 * @var RouteCollection $routes
 */
Routes::routes($routes);