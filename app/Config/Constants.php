<?php
declare(strict_types=1);

namespace Config;

require_once WSPATH . 'Config' . DIRECTORY_SEPARATOR . 'WsCfgConstants.php';

use Wellous\Ci4Component\Config\WsCfgConstants;

/**
 * Class Constants
 */
final class Constants extends WsCfgConstants
{
    /**
     * --------------------------------------------------------------------
     *  Default Constants
     * --------------------------------------------------------------------
     */
    final public static function declare(): void
    {
        parent::declare();
        self::customDeclare();
    }

    /**
     * --------------------------------------------------------------------
     *  Custom Constants
     * --------------------------------------------------------------------
     */
    final public static function customDeclare(): void
    {
        /**
         * --------------------------------------------------------------------
         * Redis Prefix for you project
         * --------------------------------------------------------------------
         */
        defined('REDIS_PREFIX') || define("REDIS_PREFIX", APP_NAME . ':' . ENVIRONMENT . ':');
        defined('SHARE_PATH') || define('SHARE_PATH', getenv('EFS_PATH') !== FALSE ? getenv('EFS_PATH') : ($_SERVER['EFS_PATH'] ?? ''));
        defined('ADMIN_ACTIONS') || define("ADMIN_ACTIONS", []);
    }
}

Constants::declare();
