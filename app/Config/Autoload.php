<?php
declare(strict_types=1);

namespace Config;

require_once WSPATH . 'Config' . DIRECTORY_SEPARATOR . 'WsCfgAutoload.php';

use Wellous\Ci4Component\Config\WsCfgAutoload;

/**
 * -------------------------------------------------------------------
 * AUTOLOADER CONFIGURATION
 * -------------------------------------------------------------------
 * This file defines the namespaces and class maps so the Autoloader
 * can find the files as needed.
 * NOTE: If you use an identical key in $psr4 or $classmap, then
 * the values in this file will overwrite the framework's values.
 */
class Autoload extends WsCfgAutoload
{
}
