<?php
declare(strict_types=1);

namespace Config;

use App\Filters\CorsFilter;
use App\Filters\AdminFilter;
use App\Filters\TransFilter;
use App\Filters\VisitorFilter;
use App\Filters\ProfilerFilter;
use App\Filters\OpenApiAuthFilter;
use Wellous\Ci4Component\Config\WsCfgFilters;

class Filters extends WsCfgFilters
{
    /**
     * Configures aliases for Filter classes to
     * make reading things nicer and simpler.
     * @var array
     */
    public array $aliases = [
        'cors'     => CorsFilter::class,
        'trans'    => TransFilter::class,
        'profiler' => ProfilerFilter::class,
        'visitor'  => VisitorFilter::class,
        'admin'    => AdminFilter::class,
        'agecy'    => AdminFilter::class,
        'openapi'  => OpenApiAuthFilter::class,
    ];

    /**
     * List of filter aliases that are always
     * applied before and after every request.
     * @var array
     */
    public array $globals = [
        'before' => [
            'cors',
            'trans',
            'profiler',
            'visitor',
            'admin' => [
                'except' => [
                    'admin/auth/*',
                ],
            ],
        ],
        'after'  => [
            'trans',
            'visitor',
            'profiler',
        ],
    ];

    /**
     * List of filter aliases that works on a
     * particular HTTP method (GET, POST, etc.).
     * Example:
     * 'post' => ['csrf', 'throttle']
     * @var array
     */
    public array $methods = [];

    /**
     * List of filter aliases that should run on any
     * before or after URI patterns.
     * Example:
     * 'isLoggedIn' => ['before' => ['account/*', 'profiles/*']]
     * @var array
     */
    public array $filters = [];
}