<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgUserAgents;

/**
 * -------------------------------------------------------------------
 * User Agents
 * -------------------------------------------------------------------
 * This file contains four arrays of user agent data. It is used by the
 * User Agent Class to help identify browser, platform, robot, and
 * mobile device data. The array keys are used to identify the device
 * and the array values are used to set the actual name of the item.
 */
class UserAgents extends WsCfgUserAgents
{
}