<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgMimes;

/**
 * Mimes
 * This file contains an array of mime types.  It is used by the
 * Upload class to help identify allowed file types.
 * When more than one variation for an extension exist (like jpg, jpeg, etc)
 * the most common one should be first in the array to aid the guess*
 * methods. The same applies when more than one mime-type exists for a
 * single extension.
 * When working with mime types, please make sure you have the ´fileinfo´
 * extension enabled to reliably detect the media types.
 */
class Mimes extends WsCfgMimes
{
}