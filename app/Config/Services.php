<?php
declare(strict_types=1);

namespace Config;

require_once WSPATH . 'Config' . DIRECTORY_SEPARATOR . 'WsCfgServices.php';

use App\Modules\Models;
use App\ThirdParty\Tool\Reporter;
use App\ThirdParty\Tool\RedisClient;
use App\Modules\Modules as AppModules;
use Wellous\Ci4Component\Config\WsCfgServices;

/**
 * Services Configuration file.
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends WsCfgServices
{
    /**
     * @return Models
     */
    public static function models(): Models
    {
        return self::modules()->models;
    }

    /**
     * @param bool $getShared
     * @return AppModules
     */
    public static function modules(bool $getShared = TRUE): AppModules
    {
        if($getShared) {
            /** @var AppModules $module */
            $module = static::getSharedInstance('modules');
            return $module;
        }
        return new AppModules();
    }

    /**
     * @param bool $getShared
     * @return RedisClient
     */
    public static function redis(bool $getShared = TRUE): RedisClient
    {
        if($getShared) {
            /** @var RedisClient $module */
            $module = static::getSharedInstance('redis');
            return $module;
        }
        $config = config('app')->{'predis'};
        return new RedisClient($config['params'], $config['options']);
    }

    /**
     * @param bool $getShared
     * @return Reporter
     */
    public static function reporter(bool $getShared = TRUE): Reporter
    {
        if($getShared) {
            /** @var Reporter $module */
            $module = static::getSharedInstance('reporter');
            return $module;
        }
        return new Reporter();
    }
}