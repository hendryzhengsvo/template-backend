<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgCache;
use CodeIgniter\Cache\Handlers\DummyHandler;

class Cache extends WsCfgCache
{
    /**
     * --------------------------------------------------------------------------
     * Primary Handler
     * --------------------------------------------------------------------------
     * The name of the preferred handler that should be used. If for some reason
     * it is not available, the $backupHandler will be used in its place.
     * @var string
     */
    public string $handler = 'dummy';

    /**
     * --------------------------------------------------------------------------
     * Available Cache Handlers
     * --------------------------------------------------------------------------
     * This is an array of cache engine alias' and class names. Only engines
     * that are listed here are allowed to be used.
     * @var array<string, string>
     */
    public array $validHandlers = [
        'dummy' => DummyHandler::class,
    ];
}
