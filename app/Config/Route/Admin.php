<?php
declare(strict_types=1);

namespace App\Config\Route;

use CodeIgniter\Router\RouteCollection;

/**
 * Class Admin Routes
 * @package App\Config\Route
 */
class Admin
{
    public static function routes(RouteCollection $routes): void
    {
        $routes->group('admin', ['namespace' => 'App\Controllers\Admin'], function(RouteCollection $routes)
        {

            /**
             * Initial Route
             */
            $routes->add('initial', 'Initial::index');

            /**
             * Auth Related Routes
             */
            $routes->group('auth', function(RouteCollection $routes)
            {
                $routes->post('login', 'Auth\\Login::doLogin');
                $routes->post('login/token', 'Auth\\Login::doLoginToken');
                $routes->post('biometric/login', 'Auth\\Login::doBioMetricLogin');
                $routes->post('logout', 'Auth\\Login::doLogout');

                $routes->post('reset/request', 'Auth\\Reset::requestReset');
                $routes->post('reset/redirect', 'Auth\\Reset::redirect');
                $routes->post('reset/confirm', 'Auth\\Reset::resetPassword');
            });

            /**
             * Notification & Web Push Related Routes
             */
            $routes->group('notification', function(RouteCollection $routes)
            {
                $routes->post('pendingList', 'Notification::pendingList');
                $routes->post('unreadCount', 'Notification::getUnreadCount');
                $routes->post('subscribeWebPush', 'Notification::subscribeWebPush');
                $routes->post('unsubscribeWebPush', 'Notification::unsubscribeWebPush');
                $routes->get('pushNotification', 'Notification::pushNotification');
                $routes->post('getByCategory', 'Notification::getByCategory');
            });

            /**
             * User Profile Related Routes
             */
            $routes->group('profile', function(RouteCollection $routes)
            {
                $routes->post('update', 'Profile::updateProfile');

                $routes->post('avatar', 'Profile::uploadAvatar');
                $routes->post('details', 'Profile::details');
                $routes->post('info', 'Profile::info');
                $routes->post('changePass', 'Security::changePass');

                /**
                 * BioMetric Related Routes
                 */
                $routes->group('biometric', function(RouteCollection $routes)
                {
                    $routes->post('add', 'Profile::addBioMetric');
                    $routes->post('remove', 'Profile::removeBioMetric');
                });
            });

            /**
             * User Action Log Related Routes
             */
            $routes->group('log', function(RouteCollection $routes)
            {
                $routes->post('listing', 'AdminLog::listing');
            });

            /**
             * todos demo app
             */
            $routes->group('todo', function(RouteCollection $routes)
            {
                $routes->post('add', 'Todo::add');
                $routes->add('list', 'Todo::listing');
                $routes->add('remove', 'Todo::remove');
                $routes->post('complete', 'Todo::complete');
                $routes->post('batchComplete', 'Todo::batchComplete');
                $routes->post('batchDelete', 'Todo::batchDelete');
                $routes->post('update', 'Todo::updateInfo');
            });
        });
    }
}