<?php
declare(strict_types=1);

namespace App\Config\Route;

use App\Filters\AgencyAuthFilter;
use CodeIgniter\Router\RouteCollection;

/**
 * Class Admin Routes
 * @package App\Config\Route
 */
class OpenApi
{
    public static function routes(RouteCollection $routes): void
    {
        $routes->group('api', [
            'namespace' => 'App\Controllers',
            'filter'    => AgencyAuthFilter::class,
        ], function(RouteCollection $routes)
        {
            $routes->group('todo', function(RouteCollection $routes)
            {
                $routes->post('add', 'OpenApi\\Todo::add');
                $routes->add('list', 'OpenApi\\Todo::listing');
                $routes->add('remove', 'OpenApi\\Todo::remove');
                $routes->post('complete', 'OpenApi\\Todo::complete');
                $routes->post('batchComplete', 'OpenApi\\Todo::batchComplete');
                $routes->post('batchDelete', 'OpenApi\\Todo::batchDelete');
                $routes->post('update', 'OpenApi\\Todo::updateInfo');
            });
        });
    }
}