<?php
declare(strict_types=1);

namespace App\Config\Route;

use CodeIgniter\Router\RouteCollection;

/**
 * Class Admin Routes
 * @package App\Config\Route
 */
class Main
{
    /**
     * Main Sample Routes
     * @param RouteCollection $routes
     * @return void
     */
    public static function routes(RouteCollection $routes): void
    {
        $routes->match(['post', 'get'], '/', 'Home::index');
    }
}