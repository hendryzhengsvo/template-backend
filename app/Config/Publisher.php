<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgPublisher;

/**
 * Publisher Configuration
 * Defines basic security restrictions for the Publisher class
 * to prevent abuse by injecting malicious files into a project.
 */
class Publisher extends WsCfgPublisher
{
}