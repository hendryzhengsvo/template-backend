<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgDatabase;

/**
 * Database Configuration
 */
class Database extends WsCfgDatabase
{
    /**
     * Lets you choose which connection group to
     * use if no other is specified.
     * @var string
     */
    public string $defaultGroup = 'master';

    /**
     * The master database connection.
     * @var array
     */
    public array $master = [
        'hostname' => DB_ACCESS['primary'],
        'username' => DB_ACCESS['user'],
        'password' => DB_ACCESS['password'],
        'database' => DB_ACCESS['database'],
    ];

    /**
     * The slave database connection.
     * @var array
     */
    public array $slave = [
        'hostname' => DB_ACCESS['primary'],
        'username' => DB_ACCESS['user'],
        'password' => DB_ACCESS['password'],
        'database' => DB_ACCESS['database'],
    ];

    /**
     * Constructor method for the class.
     * Combines the default array with the master and slave arrays
     * and assigns the result to the corresponding properties.
     * Calls the parent class constructor method.
     * @return void
     */
    final public function __construct()
    {
        $this->master = [...$this->default, ...$this->master];
        $this->slave  = [...$this->default, ...$this->slave];
        parent::__construct();
    }
}