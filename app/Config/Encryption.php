<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgEncryption;

/**
 * Encryption configuration.
 * These are the settings used for encryption, if you don't pass a parameter
 * array to the encrypter for creation/initialization.
 */
class Encryption extends WsCfgEncryption
{
    public string $key = ''; //Please update your key
}