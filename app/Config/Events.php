<?php
declare(strict_types=1);

namespace Config;

use Throwable;
use ReflectionException;
use App\Modules\Storage;
use CodeIgniter\Events\Events as CIEvents;
use Wellous\Ci4Component\Tool\WsHandleEvents;

/**
 * Class Events
 */
class Events
{
    /**
     * @return void
     */
    public static function register(): void
    {
        /*
         * --------------------------------------------------------------------
         * Application Events
         * --------------------------------------------------------------------
         * Events allow you to tap into the execution of the program without
         * modifying or extending core files. This file provides a central
         * location to define your events, though they can always be added
         * at run-time, also, if needed.
         *
         * You create code that can execute by subscribing to events with
         * the 'on()' method. This accepts any form of callable, including
         * Closures, that will be executed when the event is triggered.
         *
         * Example:
         *      CIEvents::on('create', [$myInstance, 'myMethod']);
         */
        WsHandleEvents::handlePreSystem();

        CIEvents::on('critical_error', function($error)
        {
            Services::reporter()->log("Critical Error", [
                'status' => 'critical_error',
                'app'    => APP_NAME ?? '',
                'domain' => $_SERVER['HTTP_HOST'] ?? '',
                'error'  => $error,
            ]);
        });

        CIEvents::on('exception_report', function(Throwable $ex, array $trace, string $errorMessage)
        {
            Services::reporter()->log(get_class($ex), "$errorMessage on " . implode("\n", $trace));
        });

        CIEvents::on('exception', function($ex, $response)
        {
            if(Storage::$visitor)
                $response->setHeader('VisitorId', Storage::$visitor->code);
        });

    }
}

/**
 * @param string $event
 * @param array  $params
 * @return void
 * @throws ReflectionException
 */
Events::register();