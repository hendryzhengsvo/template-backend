<?php
declare(strict_types=1);

/*
 |--------------------------------------------------------------------------
 | ENVIRONMENT
 |--------------------------------------------------------------------------
 | This can be set to anything, but default usage is:
 |
 |     live - Production environment
 |     staging - Testing/UAT environment
 |     development - Development environment
 |     local - Local development environment
 |
 | NOTE: If you change these, also change the error_reporting() code below
 */

/*
 |--------------------------------------------------------------------------
 | ERROR DISPLAY
 |--------------------------------------------------------------------------
 | In development, we want to show as many errors as possible to help
 | make sure they don't make it to production. And save us hours of
 | painful debugging.
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
ini_set('display_errors', '0');

/**
 * --------------------------------------------------------------------
 * Redis configuration for current environment
 * --------------------------------------------------------------------
 */
define('REDIS_OPTIONS', [
    'params'  => array_map(fn($host) => "tcp://$host:26379", explode(',', env(APP_NAME . '_REDIS_SENTINEL_HOSTS', '127.0.0.1'))),
    'options' => [
        'prefix'      => REDIS_PREFIX,
        'parameters'  => [
            'password' => '',
            'database' => (int)env(APP_NAME . '_REDIS_SENTINEL_DB', 10),
        ],
        'replication' => 'sentinel',
        'service'     => env(APP_NAME . '_REDIS_SENTINEL_SERVICE', 'mymaster'),
    ],
]);

/**
 * --------------------------------------------------------------------
 * Database configuration for current environment
 * --------------------------------------------------------------------
 */
define('DB_ACCESS', [
    'primary'  => env(APP_NAME . '_MYSQL_MASTER_HOST', '127.0.0.1'),
    'slave'    => env(APP_NAME . '_MYSQL_SLAVE_HOST', '127.0.0.1'),
    'user'     => env(APP_NAME . '_MYSQL_USERNAME', 'root'),
    'password' => env(APP_NAME . '_MYSQL_PASSWORD', ''),
    'database' => env(APP_NAME . '_MYSQL_DATABASE', strtolower(APP_NAME)),
]);

/**
 * --------------------------------------------------------------------
 * Cloud storage configuration for current environment
 * --------------------------------------------------------------------
 */
define('CLOUD_FILES_CONFIG', [
    'bucket'     => [
        'region' => env(APP_NAME . '_AWS_DEFAULT_REGION', 'ap-southeast-1'),
        'bucket' => env(APP_NAME . '_AWS_S3_BUCKET', 'template-assets'),
        'access' => [
            'key'    => env(APP_NAME . '_AWS_ACCESS_KEY_ID', ''),
            'secret' => env(APP_NAME . '_AWS_SECRET_ACCESS_KEY', ''),
        ],
        'path'   => '/' . strtolower(APP_NAME),
    ],
    'cloudFront' => [
        'cfDistributionId' => env(APP_NAME . '_AWS_CF_DISTRIBUTION_ID', ''),
        'access'           => [
            'key'    => env(APP_NAME . '_AWS_CF_ACCESS_KEY_ID', ''),
            'secret' => env(APP_NAME . '_AWS_CF_SECRET_ACCESS_KEY', ''),
        ],
        'domain'           => 'cdn.template.com', //Template Sample only, please change to your own domain
    ],
]);

defined('CLOUD_FILES_PATH') || define("CLOUD_FILES_PATH", [
    'adminAvatar' => CLOUD_FILES_CONFIG['bucket']['path'] . '/admin/avatar',
]);

/**
 * To determine that is production environment
 * @return bool
 */
function isProduction(): bool
{
    return TRUE;
}