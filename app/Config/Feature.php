<?php
declare(strict_types=1);

namespace Config;

use Wellous\Ci4Component\Config\WsCfgFeature;

/**
 * Enable/disable backward compatibility breaking features.
 */
class Feature extends WsCfgFeature
{
}