-- ----------------------------
-- Migrate Template SQL
-- ----------------------------

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`       varchar(100) NOT NULL,
    `nickname`   varchar(20) NULL DEFAULT NULL,
    `email`      varchar(60)  NOT NULL,
    `password`   varchar(40)  NOT NULL DEFAULT '',
    `home_route` json NULL,
    `status`     tinyint UNSIGNED NOT NULL DEFAULT 1,
    `role_id`    int UNSIGNED NOT NULL DEFAULT 1,
    `avatar_ver` smallint     NOT NULL DEFAULT 0,
    `created_at` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `updated_at` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `idx_email`(`email` ASC) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin`
VALUES (1, 'Administrator', NULL, 'admin@example.com', '9d19ddc3ae6703a72f181709e9cea828f4455b8a', NULL, 1, 1, 0,
        '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- ----------------------------
-- Table structure for admin_actions
-- ----------------------------
DROP TABLE IF EXISTS `admin_actions`;
CREATE TABLE `admin_actions`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`       varchar(30) NOT NULL DEFAULT '',
    `code`       varchar(30) NOT NULL DEFAULT '',
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `idx_code`(`code` ASC) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_actions
-- ----------------------------

-- ----------------------------
-- Table structure for admin_biometrics
-- ----------------------------
DROP TABLE IF EXISTS `admin_biometrics`;
CREATE TABLE `admin_biometrics`
(
    `id`            int UNSIGNED NOT NULL AUTO_INCREMENT,
    `admin_id`      int          NOT NULL,
    `credential_id` varchar(128) NOT NULL,
    `credential`    json         NOT NULL,
    `deleted_at`    datetime NULL DEFAULT NULL,
    `created_at`    datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `idx_member_credential_id`(`credential_id` ASC) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_biometrics
-- ----------------------------

-- ----------------------------
-- Table structure for admin_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_log`;
CREATE TABLE `admin_log`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `admin_id`   int UNSIGNED NOT NULL DEFAULT 0,
    `ref_id`     int UNSIGNED NOT NULL DEFAULT 0,
    `ref_table`  varchar(50) NOT NULL,
    `visitor_id` int UNSIGNED NOT NULL DEFAULT 0,
    `action`     varchar(50) NOT NULL DEFAULT '',
    `param`      json NULL,
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for admin_login
-- ----------------------------
DROP TABLE IF EXISTS `admin_login`;
CREATE TABLE `admin_login`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `admin_id`   int UNSIGNED NOT NULL DEFAULT 0,
    `source`     varchar(20)  NOT NULL DEFAULT 'login',
    `useragent`  varchar(250) NOT NULL,
    `browser`    varchar(50)  NOT NULL,
    `platform`   varchar(50)  NOT NULL,
    `login_ip`   varchar(40)  NOT NULL DEFAULT '',
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_login
-- ----------------------------

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `parent_id`  int UNSIGNED NOT NULL DEFAULT 0,
    `role_id`    int UNSIGNED NOT NULL DEFAULT 0,
    `icon`       varchar(30) NULL DEFAULT '',
    `name`       varchar(30) NULL DEFAULT '',
    `path`       varchar(50) NULL DEFAULT '',
    `sort`       tinyint UNSIGNED NULL DEFAULT 1,
    `status`     tinyint UNSIGNED NULL DEFAULT 1,
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu`
VALUES (1, 0, 1, 'pi-home', 'Home', 'Admin/Home', 1, 1, '2023-05-25 18:02:52', '2023-05-25 18:02:53', NULL);
INSERT INTO `admin_menu`
VALUES (2, 0, 1, 'pi-check-square', 'To-do List', 'Admin/ToDoList', 1, 1, '2023-05-25 18:02:52', '2023-05-25 18:02:53',
        NULL);


-- ----------------------------
-- Table structure for admin_notification
-- ----------------------------
DROP TABLE IF EXISTS `admin_notification`;
CREATE TABLE `admin_notification`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `admin_id`   int UNSIGNED NOT NULL DEFAULT 0,
    `category`   enum('order','task','reward','misc') NOT NULL DEFAULT 'misc',
    `title`      varchar(50)  NOT NULL DEFAULT '',
    `content`    varchar(500) NOT NULL DEFAULT '',
    `status`     int UNSIGNED NOT NULL DEFAULT 0 COMMENT '0=pending 1=viewed',
    `params`     json         NOT NULL,
    `created_at` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_notification
-- ----------------------------

-- ----------------------------
-- Table structure for admin_role_actions
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_actions`;
CREATE TABLE `admin_role_actions`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `role_id`    int UNSIGNED NULL DEFAULT 0,
    `action_id`  int UNSIGNED NULL DEFAULT 0,
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `idx_action`(`role_id` ASC, `action_id` ASC) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_role_actions
-- ----------------------------

-- ----------------------------
-- Table structure for admin_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `role_id`    int UNSIGNED NULL DEFAULT 0,
    `menu_id`    int UNSIGNED NULL DEFAULT 0,
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `idx_menu`(`role_id` ASC, `menu_id` ASC) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_role_menu
-- ----------------------------
INSERT INTO `admin_role_menu`
VALUES (1, 1, 1, '2023-05-25 18:34:17', '2023-05-25 18:34:17', NULL);
INSERT INTO `admin_role_menu`
VALUES (2, 1, 2, '2023-05-25 18:34:17', '2023-05-25 18:34:17', NULL);

-- ----------------------------
-- Table structure for admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles`
(
    `id`         int UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`       varchar(30) NOT NULL DEFAULT '',
    `code`       varchar(30) NOT NULL DEFAULT '',
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `idx_code`(`code` ASC) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_roles
-- ----------------------------
INSERT INTO `admin_roles`
VALUES (1, 'Default role', 'default_role', '2023-05-25 18:50:00', '2023-05-25 18:50:28', NULL);

-- ----------------------------
-- Table structure for admin_webpush
-- ----------------------------
CREATE TABLE `admin_web_push`
(
    `id`           int unsigned NOT NULL AUTO_INCREMENT,
    `admin_id`     int unsigned NOT NULL DEFAULT '0',
    `subscription` json     NOT NULL,
    `endpoint`     varchar(500) GENERATED ALWAYS AS (json_unquote(json_extract(`subscription`, _utf8mb4'$.endpoint'))) VIRTUAL NOT NULL,
    `enckey`       json GENERATED ALWAYS AS (json_unquote(json_extract(`subscription`, _utf8mb4'$.keys'))) VIRTUAL NOT NULL,
    `status`       tinyint unsigned NOT NULL DEFAULT '1',
    `updated_at`   datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    `created_at`   datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `idx_endpoint` (`endpoint`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of admin_web_push
-- ----------------------------

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`
(
    `id`       int UNSIGNED NOT NULL AUTO_INCREMENT,
    `type`     varchar(20)   NOT NULL,
    `platform` enum('default','admin') NOT NULL DEFAULT 'default',
    `key`      varchar(100)  NOT NULL,
    `value`    varchar(1000) NOT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of config
-- ----------------------------

-- ----------------------------
-- Table structure for frontend_config
-- ----------------------------
DROP TABLE IF EXISTS `frontend_config`;
CREATE TABLE `frontend_config`
(
    `id`       int UNSIGNED NOT NULL AUTO_INCREMENT,
    `platform` enum('admin','member') NOT NULL DEFAULT 'admin',
    `type`     varchar(20)   NOT NULL,
    `key`      varchar(100)  NOT NULL,
    `value`    varchar(1000) NOT NULL,
    `status`   tinyint UNSIGNED NOT NULL DEFAULT 1,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `idx_key`(`key` ASC) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of frontend_config
-- ----------------------------
INSERT INTO `frontend_config`
VALUES (1, 'admin', 'json-array', 'available_language', '[\"en-US\",\"zh-CN\"]', 1);
INSERT INTO `frontend_config`
VALUES (2, 'admin', 'json-array', 'pagination', '{\"pageSizeOption\":[5,10,20,50,100],\"defaultPageSize\":10}', 1);
INSERT INTO `frontend_config`
VALUES (3, 'admin', 'string', 'version', '1.0.0', 1);

-- ----------------------------
-- Table structure for oauth_connect
-- ----------------------------
DROP TABLE IF EXISTS `oauth_connect`;
CREATE TABLE `oauth_connect`
(
    `id`         bigint UNSIGNED NOT NULL AUTO_INCREMENT,
    `roleid`     bigint UNSIGNED NOT NULL DEFAULT 0,
    `panel`      varchar(20) NOT NULL DEFAULT 'admin',
    `service`    varchar(20) NOT NULL,
    `serviceid`  varchar(50) NOT NULL,
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL,
    `deleted_at` datetime NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `idx_oauth_role`(`roleid` ASC, `service` ASC) USING BTREE,
    UNIQUE INDEX `idx_oauth_service`(`panel` ASC, `service` ASC, `serviceid` ASC) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of oauth_connect
-- ----------------------------

-- ----------------------------
-- Table structure for openapi_client
-- ----------------------------
CREATE TABLE `openapi_client`
(
    `id`         int                                                          NOT NULL AUTO_INCREMENT,
    `name`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `api_key`    varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `api_secret` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `status`     tinyint unsigned NOT NULL DEFAULT '1',
    `created_at` datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of openapi_client
-- ----------------------------

-- ----------------------------
-- Table structure for visitor
-- ----------------------------
DROP TABLE IF EXISTS `visitor`;
CREATE TABLE `visitor`
(
    `id`           int UNSIGNED NOT NULL AUTO_INCREMENT,
    `code`         char(64) NOT NULL,
    `platform`     enum('admin','afyaa','member') NOT NULL DEFAULT 'member',
    `login_id`     int UNSIGNED NOT NULL DEFAULT 0,
    `status`       tinyint UNSIGNED NOT NULL DEFAULT 1,
    `logged_at`    bigint UNSIGNED NULL DEFAULT 0,
    `lastseen_at`  bigint   NOT NULL,
    `firstseen_at` bigint   NOT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of visitor
-- ----------------------------


-- ----------------------------
-- Table structure for todos
-- ----------------------------
CREATE TABLE `todos`
(
    `id`              int(10) unsigned NOT NULL AUTO_INCREMENT,
    `type`            enum('admin','agency','user') NOT NULL DEFAULT 'admin',
    `type_id`         int(10) NOT NULL DEFAULT '0',
    `title`           varchar(100) NOT NULL,
    `description`     varchar(300) DEFAULT NULL,
    `is_completed`    tinyint(1) NOT NULL DEFAULT '0',
    `completion_time` datetime     DEFAULT NULL,
    `reminder_time`   datetime     DEFAULT NULL,
    `created_at`      timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`      timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ROW_FORMAT = DYNAMIC;
SET
FOREIGN_KEY_CHECKS = 1;
ALTER TABLE `todos`
    MODIFY COLUMN `type` enum('admin','agency','user') NOT NULL DEFAULT 'admin' AFTER `id`;
-- ----------------------------
-- Records of todos
-- ----------------------------
