<?php
declare(strict_types=1);

namespace App\Libraries;

use Wellous\Ci4Component\Libraries\WsLibContainer;

/**
 * Class Container
 */
class Container extends WsLibContainer
{
}