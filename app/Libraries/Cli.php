<?php
declare(strict_types=1);

namespace App\Libraries;

use League\CLImate\Util\Output;
use League\CLImate\Decorator\Style;
use League\CLImate\Argument\Manager;
use Wellous\Ci4Component\Libraries\WsLibCli;
use League\CLImate\TerminalObject\Dynamic\Input;
use League\CLImate\TerminalObject\Dynamic\Padding;
use League\CLImate\TerminalObject\Dynamic\Spinner;

/**
 * @method Cli black(string $str = NULL)
 * @method Cli red(string $str = NULL)
 * @method Cli green(string $str = NULL)
 * @method Cli yellow(string $str = NULL)
 * @method Cli blue(string $str = NULL)
 * @method Cli magenta(string $str = NULL)
 * @method Cli cyan(string $str = NULL)
 * @method Cli lightGray(string $str = NULL)
 * @method Cli darkGray(string $str = NULL)
 * @method Cli lightRed(string $str = NULL)
 * @method Cli lightGreen(string $str = NULL)
 * @method Cli lightYellow(string $str = NULL)
 * @method Cli lightBlue(string $str = NULL)
 * @method Cli lightMagenta(string $str = NULL)
 * @method Cli lightCyan(string $str = NULL)
 * @method Cli white(string $str = NULL)
 * @method Cli backgroundBlack(string $str = NULL)
 * @method Cli backgroundRed(string $str = NULL)
 * @method Cli backgroundGreen(string $str = NULL)
 * @method Cli backgroundYellow(string $str = NULL)
 * @method Cli backgroundBlue(string $str = NULL)
 * @method Cli backgroundMagenta(string $str = NULL)
 * @method Cli backgroundCyan(string $str = NULL)
 * @method Cli backgroundLightGray(string $str = NULL)
 * @method Cli backgroundDarkGray(string $str = NULL)
 * @method Cli backgroundLightRed(string $str = NULL)
 * @method Cli backgroundLightGreen(string $str = NULL)
 * @method Cli backgroundLightYellow(string $str = NULL)
 * @method Cli backgroundLightBlue(string $str = NULL)
 * @method Cli backgroundLightMagenta(string $str = NULL)
 * @method Cli backgroundLightCyan(string $str = NULL)
 * @method Cli backgroundWhite(string $str = NULL)
 * @method Cli bold(string $str = NULL)
 * @method Cli dim(string $str = NULL)
 * @method Cli underline(string $str = NULL)
 * @method Cli blink(string $str = NULL)
 * @method Cli invert(string $str = NULL)
 * @method Cli hidden(string $str = NULL)
 * @method Cli success(string $str = NULL)
 * @method Cli error(string $str = NULL)
 * @method Cli critical(string $str = NULL)
 * @method Cli warn(string $str = NULL)
 * @method Cli comment(string $str = NULL)
 * @method Cli whisper(string $str = NULL)
 * @method Cli shout(string $str = NULL)
 * @method Cli inline(string $str)
 * @method Cli table(array $data)
 * @method Cli json(mixed $var)
 * @method Cli draw(string $art)
 * @method Cli border(string $char = NULL, integer $length = NULL)
 * @method Cli dump(mixed $var)
 * @method Cli flank(string $output, string $char = NULL, integer $length = NULL)
 * @method Padding padding(integer $length = 0, string $char = '.')
 * @method Input input(string $prompt)
 * @method Input confirm(string $prompt)
 * @method Input password(string $prompt)
 * @method Input checkboxes(string $prompt, array $options)
 * @method Input radio(string $prompt, array $options)
 * @method Cli columns(array $data, $column_count = NULL)
 * @method Cli clear()
 * @method Spinner spinner(string $label = NULL, string ...$characters = NULL)
 * @property Style   $style
 * @property Manager $arguments
 * @property Output  $output
 */
class Cli extends WsLibCli
{
}