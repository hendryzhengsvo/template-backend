<?php
declare(strict_types=1);

namespace App\Libraries;

use Wellous\Ci4Component\Libraries\WsLibUtilities;

/**
 * Class Utilities
 * @package Utils
 */
class Utilities extends WsLibUtilities
{
    /**
     * @param $uri
     * @param $silent
     * @return bool
     */
    final public static function isUriMatched($uri, $silent): bool
    {
        foreach($silent as $pattern) {
            $pattern = str_replace('*', '.*', $pattern);
            $pattern = str_replace('/', '\/', $pattern);
            $pattern = '/^' . $pattern . '$/';
            if(preg_match($pattern, $uri)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @param     $email
     * @param int $visibleChars
     * @return string
     */
    final public static function mosaicEmail($email, int $visibleChars = 3): string
    {
        $atSymbolIndex = strpos($email, '@');
        if($atSymbolIndex === FALSE) {
            // Invalid email, return the original email
            return $email;
        }

        $username        = substr($email, 0, $atSymbolIndex);
        $visibleUsername = substr($username, 0, $visibleChars);
        $mosaicUsername  = str_repeat('*', 6);

        $domain = substr($email, $atSymbolIndex + 1);

        return $visibleUsername . $mosaicUsername . '@' . $domain;
    }
}