<?php
declare(strict_types=1);

namespace App\Libraries;

use Wellous\Ci4Component\Libraries\WsLibProvider;

class Provider extends WsLibProvider
{
    public const IMG_RESIZE_SIZE_COVER   = 'cover';
    public const IMG_RESIZE_SIZE_CONTAIN = 'contain';
    public const IMG_RESIZE_SIZE_STRETCH = 'stretch';
}