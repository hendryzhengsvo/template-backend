<?php
declare(strict_types=1);

namespace App\Libraries;

use Wellous\Ci4Component\Libraries\WsLibBuilder;

/**
 * Class Builder
 * Enhanced curl and make easier to use
 */
class Builder extends WsLibBuilder
{
}