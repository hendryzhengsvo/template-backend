<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\OpenapiClientEntity;

//private-section-begin

class OpenapiClient extends Base
{
    protected $table      = 'openapi_client';
    protected $primaryKey = 'id';
    protected $returnType = OpenapiClientEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'name',
        'api_key',
        'api_secret',
        'status',
        'created_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'name'       => 'string|max_length[50]',
        'api_key'    => 'string|max_length[40]',
        'api_secret' => 'string|max_length[40]',
        'status'     => 'permit_empty|numeric',
        'created_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * Retrieve an OpenapiClientEntity by API key.
     * This method retrieves an OpenapiClientEntity object from the database by the provided API key.
     * @param string $apiKey The API key used to retrieve the OpenapiClientEntity.
     * @return OpenapiClientEntity|null The retrieved OpenapiClientEntity if found, or null if not found.
     */
    public function getByApiKey(string $apiKey): ?OpenapiClientEntity
    {
        /** @var OpenapiClientEntity $result */
        $result = $this->where('api_key', $apiKey)
            ->where('status', 1)
            ->first();
        return $result ?: NULL;
    }
}