<?php
declare(strict_types=1);

namespace App\Models;

use ReflectionException;
use App\Entities\FrontendConfigEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;

//private-section-begin

class FrontendConfig extends Base
{
    protected $table      = 'frontend_config';
    protected $primaryKey = 'id';
    protected $returnType = FrontendConfigEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'platform',
        'type',
        'key',
        'value',
        'status',
    ];

    protected $validationRules = [
        'id'       => 'permit_empty|numeric',
        'platform' => 'permit_empty|string',
        'type'     => 'string|max_length[20]',
        'key'      => 'string|max_length[100]',
        'value'    => 'string|max_length[1000]',
        'status'   => 'permit_empty|numeric',
    ];

//private-section-end

    /**
     * Get All Config
     */
    public function getAll(string $platform): array
    {
        return $this->whereIn('platform', ['default', $platform])
            ->where('status', 1)
            ->findAll();
    }

    /**
     * get entity by id
     * @param string $platform
     * @param string $key
     * @return FrontendConfigEntity|null
     */
    public function getByKey(string $platform, string $key): ?FrontendConfigEntity
    {
        /** @var FrontendConfigEntity $result */
        $result = $this->where(['platform' => $platform, 'key' => $key, 'status' => 1])->first();
        return $result ?? NULL;
    }

    /**
     * @param string $platform
     * @param string $type
     * @param string $key
     * @param string $value
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function updateConfig(string $platform, string $type, string $key, string $value): bool
    {
        return $this->set(['type' => $type, 'value' => $value])
            ->where('platform', $platform)
            ->where('key', $key)
            ->update();
    }

    /**
     * @param string $platform
     * @param string $type
     * @param string $key
     * @param string $value
     * @return int
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function createConfig(string $platform, string $type, string $key, string $value): int
    {
        return (int)$this->insert([
            'platform' => $platform,
            'type'     => $type,
            'key'      => $key,
            'value'    => $value,
            'status'   => 1,
        ]);
    }
}