<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AgencyEntity;

//private-section-begin

class Agency extends Base
{
    protected $table      = 'agency';
    protected $primaryKey = 'id';
    protected $returnType = AgencyEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'top_agency_id',
        'name',
        'gender',
        'email',
        'mobile',
        'password',
        'date_of_birth',
        'role_id',
        'verify',
        'expired',
        'avatarVer',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'            => 'permit_empty|numeric',
        'top_agency_id' => 'permit_empty|numeric',
        'name'          => 'permit_empty|string|max_length[100]',
        'gender'        => 'permit_empty|string',
        'email'         => 'permit_empty|string|max_length[100]',
        'mobile'        => 'permit_empty|string|max_length[100]',
        'password'      => 'permit_empty|string|max_length[100]',
        'date_of_birth' => 'permit_empty|string|max_length[10]',
        'role_id'       => 'permit_empty|numeric',
        'verify'        => 'permit_empty|numeric|max_length[1]',
        'expired'       => 'permit_empty|numeric',
        'avatarVer'     => 'permit_empty|numeric',
        'created_at'    => 'permit_empty|string|max_length[25]',
        'updated_at'    => 'permit_empty|string|max_length[25]',
        'deleted_at'    => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * get entity by id
     * @param int $id
     * @return AgencyEntity|null
     */
    public function getById(int $id): ?AgencyEntity
    {
        /** @var AgencyEntity $result */
        $result = $this->find($id);
        return $result ?: NULL;
    }
}