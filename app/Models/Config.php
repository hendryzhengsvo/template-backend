<?php
declare(strict_types=1);

namespace App\Models;

use ReflectionException;
use App\Entities\ConfigEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;

//private-section-begin

class Config extends Base
{
    protected $table      = 'config';
    protected $primaryKey = 'id';
    protected $returnType = ConfigEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'type',
        'platform',
        'key',
        'value',
    ];

    protected $validationRules = [
        'id'       => 'permit_empty|numeric',
        'type'     => 'string|max_length[20]',
        'platform' => 'permit_empty|string',
        'key'      => 'string|max_length[100]',
        'value'    => 'string|max_length[1000]',
    ];

//private-section-end

    /**
     * Get All Config
     */
    public function getAllByPlatform($platform = 'default'): array
    {
        return $this->where('platform', $platform)->findAll();
    }

    /**
     * get entity by id
     * @param string $key
     * @return ConfigEntity|null
     */
    public function getByKey(string $key): ?ConfigEntity
    {
        /** @var ConfigEntity $result */
        $result = $this->where('key', $key)->first();
        return $result ?? NULL;
    }

    /**
     * @param $platform
     * @param $key
     * @return ConfigEntity|null
     */
    public function getByPlatformKey($platform, $key): ?ConfigEntity
    {
        /** @var ConfigEntity $result */
        $result = $this->where('platform', $platform)->where('key', $key)->first();
        if(empty($result))
            /** @var ConfigEntity $result */
            $result = $this->where('platform', 'default')->where('key', $key)->first();

        return $result ?? NULL;
    }

    /**
     * @param ConfigEntity $config
     * @return false|int|object|string
     * @throws ReflectionException
     * @throws DbQueryError
     */
    public function add(ConfigEntity $config): object|bool|int|string
    {
        return $this->insert($config);
    }

    /**
     * @param string $type
     * @param string $key
     * @param string $value
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function updateConfig(string $type, string $key, string $value): bool
    {
        return $this->set(['type' => $type, 'value' => $value])
            ->where('key', $key)
            ->update();
    }

    /**
     * @param string $type
     * @param string $key
     * @param string $value
     * @return int
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function createConfig(string $type, string $key, string $value): int
    {
        return (int)$this->insert([
            'type'  => $type,
            'key'   => $key,
            'value' => $value,
        ]);
    }
}