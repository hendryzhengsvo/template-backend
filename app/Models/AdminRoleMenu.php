<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AdminRoleMenuEntity;

//private-section-begin

class AdminRoleMenu extends Base
{
    protected $table      = 'admin_role_menu';
    protected $primaryKey = 'id';
    protected $returnType = AdminRoleMenuEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'role_id',
        'menu_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'role_id'    => 'permit_empty|numeric',
        'menu_id'    => 'permit_empty|numeric',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
        'deleted_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * @param int $roleId
     * @return array
     */
    public function getMenuIdsByRoleId(int $roleId): array
    {
        return $this->where('role_id', $roleId)->findColumn('menu_id') ?: [];
    }
}