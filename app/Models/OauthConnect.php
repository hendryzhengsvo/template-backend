<?php
declare(strict_types=1);

namespace App\Models;

use ReflectionException;
use App\Libraries\DateTimeMethod;
use CodeIgniter\Database\BaseResult;
use App\Entities\OauthConnectEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;

//private-section-begin

class OauthConnect extends Base
{
    protected $table      = 'oauth_connect';
    protected $primaryKey = 'id';
    protected $returnType = OauthConnectEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'roleid',
        'panel',
        'service',
        'serviceid',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'roleid'     => 'permit_empty|numeric',
        'panel'      => 'permit_empty|string|max_length[20]',
        'service'    => 'string|max_length[20]',
        'serviceid'  => 'string|max_length[50]',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
        'deleted_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * @param $roleId
     * @param $service
     * @param $serviceId
     * @return bool|int|string
     * @throws ReflectionException
     * @throws DbQueryError
     */
    final public function create($roleId, $service, $serviceId): bool|int|string
    {
        return $this->insert([
            'roleid'    => $roleId,
            'service'   => $service,
            'serviceid' => $serviceId,
            'created'   => DateTimeMethod::datetime(),
            'createdts' => DateTimeMethod::timestamp(),
        ]);
    }

    final public function remove($roleId, $service): bool|BaseResult
    {
        return $this->where([
            'roleid'  => $roleId,
            'service' => $service,
        ])->delete();
    }

    /**
     * @param $party
     * @param $partyId
     * @return OauthConnectEntity|null
     */
    final public function getByParty($party, $partyId): ?OauthConnectEntity
    {
        /** @var OauthConnectEntity $result */
        $result = $this->where([
            'service'   => $party,
            'serviceid' => $partyId,
        ])->first();
        return $result ?: NULL;
    }

    /**
     * @param $roleId
     * @return array
     */
    final public function listServiceByRoleId($roleId): array
    {
        return $this->where(['roleid' => $roleId])->findColumn('service');
    }
}