<?php
declare(strict_types=1);

namespace App\Models;

use ReflectionException;
use App\Entities\TodosEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;

//private-section-begin

class Todos extends Base
{
    protected $table      = 'todos';
    protected $primaryKey = 'id';
    protected $returnType = TodosEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'type',
        'type_id',
        'title',
        'description',
        'is_completed',
        'completion_time',
        'reminder_time',
        'created_at',
        'updated_at',
    ];

    protected $validationRules = [
        'id'              => 'permit_empty|numeric',
        'type'            => 'permit_empty|string',
        'type_id'         => 'permit_empty|numeric',
        'title'           => 'string|max_length[100]',
        'description'     => 'permit_empty|string|max_length[300]',
        'is_completed'    => 'permit_empty|numeric|max_length[1]',
        'completion_time' => 'permit_empty|string|max_length[25]',
        'reminder_time'   => 'permit_empty|string|max_length[25]',
        'created_at'      => 'permit_empty|string',
        'updated_at'      => 'permit_empty|string',
    ];

//private-section-end

    /**
     * get entity by id
     * @param int $id
     * @return TodosEntity|null
     */
    public function getById(int $id): ?TodosEntity
    {
        /** @var TodosEntity $result */
        $result = $this->find($id);
        return $result ?: NULL;
    }

    /**
     * @param string   $type
     * @param int      $typeId
     * @param int|null $completed
     * @param string   $keyword
     * @param string   $orderBy
     * @param int      $limit
     * @param int      $offset
     * @param bool     $count
     * @return array|int|string
     */
    public function getListByType(string $type, int $typeId, int|null $completed, string $keyword = '', string $orderBy = 'asc', int $limit = 10,
                                  int    $offset = 0, bool $count = FALSE): array|int|string
    {
        $this->where(['type' => $type, 'type_id' => $typeId]);
        if($keyword) $this->like('title', $keyword);
        if($completed !== NULL) $this->where('is_completed', $completed);
        $this->orderBy('created_at', $orderBy);
        return $count ? $this->countAllResults() : ($this->findAll($limit, $offset) ?: []);
    }

    /**
     * @param string $type
     * @param int    $typeId
     * @param array  $ids
     * @param int    $isCompleted
     * @return false|int
     * @throws ReflectionException
     * @throws DbQueryError
     */
    public function batchCompleteByType(string $type, int $typeId, array $ids, int $isCompleted): bool|int
    {
        $result = $this->where(['type' => $type, 'type_id' => $typeId])
            ->whereIn('id', $ids)->set('is_completed', $isCompleted)->update();
        if($result)
            return $this->db->affectedRows();
        return FALSE;
    }

    /**
     * @param string $type
     * @param int    $typeId
     * @param array  $ids
     * @return false|int
     */
    public function batchDeleteByType(string $type, int $typeId, array $ids): bool|int
    {
        $result = $this->where(['type' => $type, 'type_id' => $typeId])
            ->whereIn('id', $ids)->delete();
        if($result)
            return $this->db->affectedRows();
        return FALSE;
    }
}