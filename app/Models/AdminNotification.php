<?php
declare(strict_types=1);

namespace App\Models;

use ReflectionException;
use App\Entities\AdminNotificationEntity;
use Wellous\Ci4Component\Exceptions\DbQueryError;

//private-section-begin

class AdminNotification extends Base
{
    protected $table      = 'admin_notification';
    protected $primaryKey = 'id';
    protected $returnType = AdminNotificationEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'admin_id',
        'category',
        'title',
        'content',
        'status',
        'params',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'admin_id'   => 'permit_empty|numeric',
        'category'   => 'permit_empty|string',
        'title'      => 'permit_empty|string|max_length[50]',
        'content'    => 'permit_empty|string|max_length[500]',
        'status'     => 'permit_empty|numeric',
        'params'     => 'valid_json',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
        'deleted_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * @param int $adminId
     * @param int $limit
     * @param int $offset
     * @return array|null
     */
    public function getPendingList(int $adminId, int $limit = 0, int $offset = 0): ?array
    {
        return $this->select('id,category,title,content,created_at')
            ->where('admin_id', $adminId)
            ->where('status', 0)
            ->orderBy('created_at', 'desc')
            ->findAll($limit, $offset);
    }

    /**
     * @param int $adminId
     * @return array|null
     */
    public function getUnreadCount(int $adminId): ?array
    {
        return $this
            ->where('admin_id', $adminId)
            ->where('status', 0)
            ->groupBy('category')
            ->findColumn('count(*)', 'category');
    }

    /**
     * @param int    $adminId
     * @param string $category
     * @return bool
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function markAsRead(int $adminId, string $category): bool
    {
        return $this->where('admin_id', $adminId)
            ->where('category', $category)
            ->where('status', 0)
            ->set(['status' => 1])
            ->update();
    }

    /**
     * @param int    $adminId
     * @param string $category
     * @return AdminNotificationEntity|null
     */
    public function getAdminLastContent(int $adminId, string $category): ?AdminNotificationEntity
    {
        /** @var AdminNotificationEntity $result */
        $result = $this->where('admin_id', $adminId)
            ->where('category', $category)
            ->orderBy('created_at', 'desc')
            ->first();
        return $result ?: NULL;
    }

    /**
     * @param int    $adminId
     * @param string $category
     * @param string $title
     * @param string $content
     * @param array  $params
     * @return int
     * @throws DbQueryError
     * @throws ReflectionException
     */
    public function create(int $adminId, string $category, string $title, string $content, array $params = []): int
    {
        return (int)$this->insert([
            'admin_id' => $adminId,
            'category' => $category,
            'title'    => $title,
            'content'  => $content,
            'status'   => 0,
            'params'   => json_encode($params),
        ]);
    }

    /**
     * @param int    $adminId
     * @param string $category
     * @param int    $limit
     * @param int    $offset
     * @return array|null
     */
    public function getByCategory(int $adminId, string $category, int $limit = 0, int $offset = 0): ?array
    {
        return $this->select('id,category,title,content,created_at,status,params')
            ->where('admin_id', $adminId)
            ->where('category', $category)
            ->orderBy('created_at', 'desc')
            ->findAll($limit, $offset);
    }

    public function getCountByCategory(int $adminId, string $category): int
    {
        return $this->where('admin_id', $adminId)
            ->where('category', $category)
            ->countAllResults();
    }
}