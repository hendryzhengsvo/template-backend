<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AdminWebPushEntity;

//private-section-begin

class AdminWebPush extends Base
{
    protected $table      = 'admin_web_push';
    protected $primaryKey = 'id';
    protected $returnType = AdminWebPushEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'admin_id',
        'subscription',
        'endpoint',
        'enckey',
        'status',
        'updated_at',
        'created_at',
    ];

    protected $validationRules = [
        'id'           => 'permit_empty|numeric',
        'admin_id'     => 'permit_empty|numeric',
        'subscription' => 'valid_json',
        'endpoint'     => 'string|max_length[500]',
        'enckey'       => 'valid_json',
        'status'       => 'permit_empty|numeric',
        'updated_at'   => 'string|max_length[25]',
        'created_at'   => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * @param string $endpoint
     * @return AdminWebPushEntity|null
     */
    public function get(string $endpoint): ?AdminWebPushEntity
    {
        /** @var AdminWebPushEntity $result */
        $result = $this->where(['endpoint' => $endpoint])->first();
        return $result ?: NULL;
    }

    /**
     * @param int $adminId
     * @return bool
     */
    public function deleteByAdminId(int $adminId): bool
    {
        return $this->where(['adminid' => $adminId])->delete();
    }
}