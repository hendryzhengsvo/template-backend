<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AdminEntity;

//private-section-begin

class Admin extends Base
{
    protected $table      = 'admin';
    protected $primaryKey = 'id';
    protected $returnType = AdminEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'name',
        'nickname',
        'email',
        'password',
        'home_route',
        'status',
        'role_id',
        'avatar_ver',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'name'       => 'string|max_length[100]',
        'nickname'   => 'permit_empty|string|max_length[20]',
        'email'      => 'string|max_length[60]',
        'password'   => 'permit_empty|string|max_length[40]',
        'home_route' => 'permit_empty|valid_json',
        'status'     => 'permit_empty|numeric',
        'role_id'    => 'permit_empty|numeric',
        'avatar_ver' => 'permit_empty|numeric',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
        'deleted_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * get entity by id
     * @param int $id
     * @return AdminEntity|null
     */
    public function getById(int $id): ?AdminEntity
    {
        /** @var AdminEntity $result */
        $result = $this->find($id);
        return $result ?: NULL;
    }

    public function getByLogin(string $value, string $key = 'email'): ?AdminEntity
    {
        /** @var AdminEntity $result */
        $result = $this->where([$key => $value])->first();
        return $result ?: NULL;
    }

    /**
     * @return array|null
     */
    public function getAllAdminListing(): ?array
    {
        return $this->select('id, name, nickname, email, home_route, status, role_id, avatar_ver')
            ->asArray()
            ->findAll(0, 0, 'id');
    }
}