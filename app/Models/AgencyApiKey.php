<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AgencyApiKeyEntity;

//private-section-begin

class AgencyApiKey extends Base
{
    protected $table      = 'agency_api_key';
    protected $primaryKey = 'id';
    protected $returnType = AgencyApiKeyEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'agency_id',
        'api_key',
        'api_secret',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'agency_id'  => 'permit_empty|numeric',
        'api_key'    => 'permit_empty|string|max_length[20]',
        'api_secret' => 'permit_empty|string|max_length[40]',
        'status'     => 'permit_empty|numeric|max_length[1]',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
        'deleted_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * get entity by apikey
     * @param string $key
     * @return AgencyApiKeyEntity|null
     */
    public function getByApiKey(string $key): ?AgencyApiKeyEntity
    {
        /** @var AgencyApiKeyEntity $result */
        $result = $this->where('api_key', $key)->first();
        return $result ?: NULL;
    }
}