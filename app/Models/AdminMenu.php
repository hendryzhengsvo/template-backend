<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AdminMenuEntity;

//private-section-begin

class AdminMenu extends Base
{
    protected $table      = 'admin_menu';
    protected $primaryKey = 'id';
    protected $returnType = AdminMenuEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'parent_id',
        'role_id',
        'icon',
        'name',
        'path',
        'sort',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'parent_id'  => 'permit_empty|numeric',
        'role_id'    => 'permit_empty|numeric',
        'icon'       => 'permit_empty|string|max_length[30]',
        'name'       => 'permit_empty|string|max_length[30]',
        'path'       => 'permit_empty|string|max_length[50]',
        'sort'       => 'permit_empty|numeric',
        'status'     => 'permit_empty|numeric',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
        'deleted_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * @param array $actionIds
     * @return array
     */
    public function getMenuItemsByIds(array $actionIds): array
    {
        return $this->whereIn('id', $actionIds)
            ->select('id,parent_id,name,path,icon,status,sort')
            ->orderBy('parent_id', 'asc')
            ->orderBy('sort', 'desc')
            ->where('status', 1)
            ->findAll() ?: [];
    }
}