<?php
declare(strict_types=1);

namespace App\Models;

use ReflectionException;
use App\Entities\AdminLogEntity;
use App\Libraries\DateTimeMethod;
use Wellous\Ci4Component\Exceptions\DbQueryError;

//private-section-begin

class AdminLog extends Base
{
    protected $table      = 'admin_log';
    protected $primaryKey = 'id';
    protected $returnType = AdminLogEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'admin_id',
        'ref_id',
        'ref_table',
        'visitor_id',
        'action',
        'param',
        'created_at',
        'updated_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'admin_id'   => 'permit_empty|numeric',
        'ref_id'     => 'permit_empty|numeric',
        'ref_table'  => 'string|max_length[50]',
        'visitor_id' => 'permit_empty|numeric',
        'action'     => 'permit_empty|string|max_length[50]',
        'param'      => 'permit_empty|valid_json',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * get entity by id
     * @param int $id
     * @return AdminLogEntity|null
     */
    public function getById(int $id): ?AdminLogEntity
    {
        /** @var AdminLogEntity $result */
        $result = $this->find($id);
        return $result ?: NULL;
    }

    /**
     * @param int    $adminId
     * @param int    $refId
     * @param string $table
     * @param int    $visitorId
     * @param string $action
     * @param array  $params
     * @return int
     * @throws ReflectionException
     * @throws DbQueryError
     */
    public function create(int $adminId, int $refId, string $table, int $visitorId, string $action, array $params): int
    {
        return (int)$this->insert([
            'admin_id'   => $adminId,
            'ref_id'     => $refId,
            'ref_table'  => $table,
            'visitor_id' => $visitorId,
            'action'     => $action,
            'param'      => json_encode($params),
            'created_at' => DateTimeMethod::datetime(),
            'updated_at' => DateTimeMethod::datetime(),
        ]);
    }

    /**
     * @param $table
     * @param $refId
     * @return AdminLogEntity|null
     */
    public function getLastUpdatedByRef($table, $refId): ?AdminLogEntity
    {
        /**
         * @var AdminLogEntity $result
         */
        $result = $this->where('ref_id', $refId)->where('ref_table', $table)->orderBy('created_at', 'DESC')->first();
        return $result ?: NULL;
    }

    /**
     * @param array $filter
     * @param int   $limit
     * @param int   $offset
     * @return array|null
     */
    public function getAdminListing(array $filter = [], int $limit = 10, int $offset = 0): ?array
    {
        foreach($filter as $key => $value)
            if(!empty($value) && $value !== 'all')
                $this->where($key, $value);
        return $this->findAll($limit, $offset) ?: [];
    }

    /**
     * @param array $filter
     * @return int|string
     */
    public function getAdminListingTotal(array $filter = []): int|string
    {
        foreach($filter as $key => $value)
            if(!empty($value) && $value !== 'all')
                $this->where($key, $value);
        return $this->countAllResults();
    }
}