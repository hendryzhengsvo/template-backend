<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AdminActionsEntity;

//private-section-begin

class AdminActions extends Base
{
    protected $table      = 'admin_actions';
    protected $primaryKey = 'id';
    protected $returnType = AdminActionsEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'name',
        'code',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'name'       => 'permit_empty|string|max_length[30]',
        'code'       => 'permit_empty|string|max_length[30]',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
        'deleted_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    /**
     * @param array $actionIds
     * @return array
     */
    public function getActionByIds(array $actionIds): array
    {
        return $this->whereIn('id', $actionIds)->findColumn('code') ?: [];
    }
}