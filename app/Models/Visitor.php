<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\VisitorEntity;

//private-section-begin

class Visitor extends Base
{
    protected $table      = 'visitor';
    protected $primaryKey = 'id';
    protected $returnType = VisitorEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'code',
        'platform',
        'login_id',
        'status',
        'logged_at',
        'lastseen_at',
        'firstseen_at',
    ];

    protected $validationRules = [
        'id'           => 'permit_empty|numeric',
        'code'         => 'string|max_length[64]',
        'platform'     => 'permit_empty|string',
        'login_id'     => 'permit_empty|numeric',
        'status'       => 'permit_empty|numeric',
        'logged_at'    => 'permit_empty|numeric',
        'lastseen_at'  => 'numeric',
        'firstseen_at' => 'numeric',
    ];

//private-section-end

    /**
     * get entity by code
     * @param string $code
     * @return VisitorEntity|null
     */
    public function getByCode(string $code): ?VisitorEntity
    {
        /** @var VisitorEntity $result */
        $result = $this->where(['code' => $code])->first();
        return $result ?: NULL;
    }
}