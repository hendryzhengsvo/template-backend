<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AdminLoginEntity;

//private-section-begin

class AdminLogin extends Base
{
    protected $table      = 'admin_login';
    protected $primaryKey = 'id';
    protected $returnType = AdminLoginEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'admin_id',
        'source',
        'useragent',
        'browser',
        'platform',
        'login_ip',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $validationRules = [
        'id'         => 'permit_empty|numeric',
        'admin_id'   => 'permit_empty|numeric',
        'source'     => 'permit_empty|string|max_length[20]',
        'useragent'  => 'string|max_length[250]',
        'browser'    => 'string|max_length[50]',
        'platform'   => 'string|max_length[50]',
        'login_ip'   => 'permit_empty|string|max_length[40]',
        'created_at' => 'permit_empty|string|max_length[25]',
        'updated_at' => 'permit_empty|string|max_length[25]',
        'deleted_at' => 'permit_empty|string|max_length[25]',
    ];

//private-section-end
}