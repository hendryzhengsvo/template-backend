<?php
declare(strict_types=1);

namespace App\Models;

use App\Entities\AdminBiometricsEntity;

//private-section-begin

class AdminBiometrics extends Base
{
    protected $table      = 'admin_biometrics';
    protected $primaryKey = 'id';
    protected $returnType = AdminBiometricsEntity::class;

    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationMessages = [];
    protected $skipValidation     = FALSE;
    protected $useAutoIncrement   = TRUE;
    protected $useSoftDeletes     = FALSE;
    protected $useTimestamps      = FALSE;
    protected $dateFormat         = 'datetime';
    protected $allowedFields      = [
        'id',
        'admin_id',
        'credential_id',
        'credential',
        'deleted_at',
        'created_at',
    ];

    protected $validationRules = [
        'id'            => 'permit_empty|numeric',
        'admin_id'      => 'numeric',
        'credential_id' => 'string|max_length[128]',
        'credential'    => 'valid_json',
        'deleted_at'    => 'permit_empty|string|max_length[25]',
        'created_at'    => 'permit_empty|string|max_length[25]',
    ];

//private-section-end

    public function getByCredentialId(string $credentialId): ?AdminBiometricsEntity
    {
        /** @var AdminBiometricsEntity $result */
        $result = $this->where('credential_id', $credentialId)->first();
        return $result ?: NULL;
    }

    /**
     * @param int    $meberId
     * @param string $credentialId
     * @return bool
     */
    public function deleteByCredentialId(int $meberId, string $credentialId): bool
    {
        return (boolean)$this->where([
            'credential_id' => $credentialId,
            'admin_id'      => $meberId])
            ->delete();
    }
}