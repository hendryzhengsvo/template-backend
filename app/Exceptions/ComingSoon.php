<?php
declare(strict_types=1);

namespace App\Exceptions;

class ComingSoon extends CtrlException
{
    protected bool   $needLog    = FALSE;
    protected int    $status     = 503;
    protected string $statusName = 'comingsoon';
    protected string $statusText = 'Exceptions.Coming soon';
}