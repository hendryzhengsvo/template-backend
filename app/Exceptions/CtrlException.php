<?php
declare(strict_types=1);

namespace App\Exceptions;

use Wellous\Ci4Component\Exceptions\WsExcepCtrl;

/**
 * Class CtrlException
 */
class CtrlException extends WsExcepCtrl
{
}