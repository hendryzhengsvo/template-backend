<?php
declare(strict_types=1);

namespace App\Exceptions;

class Maintenance extends CtrlException
{
    protected bool   $needLog    = FALSE;
    protected int    $status     = 503;
    protected string $statusName = 'maintenance';
    protected string $statusText = 'Exceptions.Service Maintenance';
}