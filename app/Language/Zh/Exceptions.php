<?php
declare(strict_types=1);

namespace App\Language\Zh;

use Wellous\Ci4Component\Language\Zh\WsLangExceptions;

class Exceptions extends WsLangExceptions
{

}

return Exceptions::get();