<?php
declare(strict_types=1);

namespace App\Language\En;

use Wellous\Ci4Component\Language\En\WsLangExceptions;

class Exceptions extends WsLangExceptions
{

}

return Exceptions::get();